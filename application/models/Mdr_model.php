<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Mdr_model extends Eloquent {

    protected $table = 'mdr';
    public $timestamps = false;

}
