<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class User_log_model extends Eloquent {

    protected $table = 'user_log';
    public $timestamps = false;

}