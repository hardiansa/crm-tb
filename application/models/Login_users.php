<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Login_users extends Eloquent {

    protected $table = 'users';
    public $timestamps = false;

    function login($uname, $upass) {
        $pass = MD5($upass);
        $users = $this->where([
                ['username', '=', $uname],
                ['password', '=', $pass]
        ]);
        if ($users->count() == 1) {
            return $users->first();
        } else {
            return false;
        }
    }

    function update_last_log($id) {
        date_default_timezone_set('Asia/Jakarta');
        $datetime = date("Y-m-d h:i:s");
        $user = $this->where([
                ['id_user', '=', $id]
        ])->update(['last_login' => $datetime]);
        if ($user) {
            return true;
        } else {
            return false;
        }
    }

}

//Class Login_users extends CI_Model {
//
//    function login($username, $password) {
//        $pass = MD5($password);
//        $sql = "select * from users
//                    where username = '$username' and password = '$pass'
//                    LIMIT 1";
//        $query = $this->db->query($sql);
//
//        if ($query->num_rows() == 1) {
//            return $query->result();
//        } else {
//            return false;
//        }
//    }
//
//    function update($id) {
//        $return = FALSE;
//        date_default_timezone_set('Asia/Jakarta');
//        $datetime = date("Y-m-d h:i:s");
//        $sql = "UPDATE users SET last_login = '$datetime' WHERE ID_USER = '$id'";
//        $query = $this->db->query($sql);
//        $return = TRUE;
//        return $return;
//    }
//
//}