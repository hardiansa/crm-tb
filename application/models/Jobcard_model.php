<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Jobcard_model extends Eloquent {

    protected $table = 'jobcard';
    public $timestamps = false;

}
