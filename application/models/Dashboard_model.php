<?php

Class Dashboard_model extends CI_Model {


    function getTotalJCOpen() {
        $query = $this->db->select('*')
                ->from('order_list')
                ->where('order_type', "GA01")
                ->where('PROGRESS_STATUS_ID', "1")
                ->get();
        return $query->num_rows();
    }

    function getTotalMDROpen() {
        $query = $this->db->select('*')
                ->from('order_list')
                ->where('order_type', "GA02")
                ->where('PROGRESS_STATUS_ID', "1")
                ->get();
        return $query->num_rows();
    }

      function getTotalMDROpen_excel() {
        $query = $this->db->select('*')
                ->from('mdr')
                ->where('status', "Open")

                ->get();
        return $query->num_rows();
    }

    function getTotalJChangar() {
        $query = $this->db->select('*')
                ->from('order_list')
                ->where('order_type', "GA01")
                ->where('PROGRESS_STATUS_ID', "2")
                ->get();
        return $query->num_rows();
    }

    function getTotalMDRhangar() {
        $query = $this->db->select('*')
                ->from('order_list')
                ->where('order_type', "GA02")
                ->where('PROGRESS_STATUS_ID', "2")
                ->get();
        return $query->num_rows();
    }

    function getTotalJCwsss() {
        $query = $this->db->select('*')
                ->from('order_list')
                ->where('order_type', "GA01")
                ->where('PROGRESS_STATUS_ID', "3")
                ->get();
        return $query->num_rows();
    }

    function getTotalMDRwsss() {
        $query = $this->db->select('*')
                ->from('order_list')
                ->where('order_type', "GA02")
                ->where('PROGRESS_STATUS_ID', "3")
                ->get();
        return $query->num_rows();
    }

    function getTotalJCwsse() {
        $query = $this->db->select('*')
                ->from('order_list')
                ->where('order_type', "GA01")
                ->where('PROGRESS_STATUS_ID', "4")
                ->get();
        return $query->num_rows();
    }

    function getTotalMDRwsse() {
        $query = $this->db->select('*')
                ->from('order_list')
                ->where('order_type', "GA02")
                ->where('PROGRESS_STATUS_ID', "4")
                ->get();
        return $query->num_rows();
    }

    function getTotalJCwscb() {
        $query = $this->db->select('*')
                ->from('order_list')
                ->where('order_type', "GA01")
                ->where('PROGRESS_STATUS_ID', "5")
                ->get();
        return $query->num_rows();
    }

    function getTotalMDRwscb() {
        $query = $this->db->select('*')
                ->from('order_list')
                ->where('order_type', "GA02")
                ->where('PROGRESS_STATUS_ID', "5")
                ->get();
        return $query->num_rows();
    }

    function getTotalJCwsls() {
        $query = $this->db->select('*')
                ->from('order_list')
                ->where('order_type', "GA01")
                ->where('PROGRESS_STATUS_ID', "6")
                ->get();
        return $query->num_rows();
    }

    function getTotalMDRwsls() {
        $query = $this->db->select('*')
                ->from('order_list')
                ->where('order_type', "GA02")
                ->where('PROGRESS_STATUS_ID', "6")
                ->get();
        return $query->num_rows();
    }



    function getTotalClose() {
        $query = $this->db->select('*')
                ->from('order_list')
                ->where('PROGRESS_STATUS_ID', "7")
                ->get();
        return $query->num_rows();
    }

    function getTotalMDRclose() {
        $query = $this->db->select('*')
                ->from('order_list')
                ->where('order_type', "GA02")
                ->where('PROGRESS_STATUS_ID', "7")
                ->get();
        return $query->num_rows();
    }

    function getTotalMDRclose_excel() {
        $query = $this->db->select('*')
                ->from('mdr')
                ->where('status', "close")

                ->get();
        return $query->num_rows();
    }

    function getTotalJCclose() {
        $query = $this->db->select('*')
                ->from('order_list')
                ->where('order_type', "GA01")
                ->where('PROGRESS_STATUS_ID', "7")
                ->get();
        return $query->num_rows();
    }

    function getTotalOpen() {
        $query = $this->db->select('*')
                ->from('order_list')
                ->where('PROGRESS_STATUS_ID', "1")
                ->get();
        return $query->num_rows();
    }

    function getTotalProgress() {
        $status = array('2', '3', '4', '5', '6');
        $query = $this->db->select('*')
                ->from('order_list')
                ->where_in('PROGRESS_STATUS_ID', $status)
                ->get();
        return $query->num_rows();
    }

    function getTotalJCProgress() {
        $status = array('2', '3', '4', '5', '6');
        $query = $this->db->select('*')
                ->from('order_list')
                ->where_in('PROGRESS_STATUS_ID', $status)
                ->where('order_type', "GA01")
                ->get();
        return $query->num_rows();
    }

    function getTotalMDRProgress() {
        $status = array('2', '3', '4', '5', '6');
        $query = $this->db->select('*')
                ->from('order_list')
                ->where_in('PROGRESS_STATUS_ID', $status)
                ->where('order_type', "GA02")
                ->get();
        return $query->num_rows();
    }


     function getTotalMDRProgress_excel() {


        $query = $this->db->select('*')
                ->from('mdr')
                ->where_in('status', "Progress")

                ->get();

        return $query->num_rows();
    }
    function getTotalJC(){
        $status = array('1', '2', '3', '4', '5', '6', '7');
        $query = $this->db->select('*')
                ->from('order_list')
                ->where_in('PROGRESS_STATUS_ID', $status)
                ->where('order_type', "GA01")
                ->get();
        return $query->num_rows();
    }

    function getTotalMDR(){
        $status = array('1', '2', '3', '4', '5', '6', '7');
        $query = $this->db->select('*')
                ->from('order_list')
                ->where_in('PROGRESS_STATUS_ID', $status)
                ->where('order_type', "GA02")
                ->get();
        return $query->num_rows();
    }

    function getTotalMDR_excel(){

        $query = $this->db->select('*')
                ->from('mdr')
         
                ->get();
        return $query->num_rows();
    }

    function getTotalMDR_excel_all($row){

        $query = $this->db->select('*')
                ->from('mdr')
                ->where('id_project',$row)
                ->get();
        return $query->num_rows();
    }

    function getTotalMDR_excel_close_all($row){

        $query = $this->db->select('*')
                ->from('mdr')
                ->where('id_project',$row)
                ->where('status', "close")
                ->get();
        return $query->num_rows();
    }

    function getTotalJOBCARD_excel_all($row){

        $query = $this->db->select('*')
                ->from('jobcard')
                ->where('id_project',$row)
                ->get();
        return $query->num_rows();
    }

    function getTotalJOBCARD_excel_close_all($row){

        $query = $this->db->select('*')
                ->from('jobcard')
                ->where('id_project',$row)
                ->where('status', "close")
                ->get();
        return $query->num_rows();
    }

    function getTimelineHangar1(){
        $query = $this->db->select('a.*, b.REVISION, b.PROGRESS_STATUS_ID')
                ->from('project_panning a')
                ->join('order_list b on b.REVISION = a.REVISION')
                ->where('b.PROGRESS_STATUS_ID', "2")
                ->get();
        return $query->results();
    }

    //for model ticket
    function getListticket() {
        $query = $this->db->query("SELECT * from ticket");

        if ($query->num_rows() > 0) {
            $no = 1;
            $ret = "";
            foreach ($query->result() as $row) {

                $color = "";
                $ret .= "
                    <tr>
                       <td style='background:$color'>$no</td>
                       <td>$row->subject</td>
                        <td>$row->department</td>
                         <td>$row->id_project</td>
                         <td>$row->service</td>
                         <td>$row->priority</td>
                             <td>$row->status</td>
                       <td>$row->last_replay</td>

                    </tr>
                    ";
                $no++;
            }
            return $ret;
        } else {
            return "";
        }
    }

    //for model project
    function getListproject1() {
        $query = $this->db->query("SELECT customer_name,aircraft_registered,project_name,start_date,finish_date,location,status_project,type_project from project");

        if ($query->num_rows() > 0) {
            $no = 1;
            $ret = "";
            foreach ($query->result() as $row) {

                $color = "";
                $ret .= "
                    <tr>
                       <td>$no</td>
                       <td>$row->customer_name</td>
                        <td>$row->aircraft_registered</td>
                         <td>$row->project_name</td>

                         <td>$row->start_date</td>
                         <td>$row->finish_date</td>
                         <td>$row->location</td>
                          <td>$row->status_project</td>
                           <td>$row->type_project</td>

                    </tr>
                    ";
                $no++;
            }
            return $ret;
        } else {
            return "";
        }
    }

    function getlistProject() {

        $query = $this->db->select('customer_name,aircraft_registered,project_name,description,start_date,finish_date,location,status_project')
                ->from('project')
                ->get();

        return $query->result();
    }

    //for model production_planning
 function getListDataProject() {
        $query = $this->db->query("select REVISION,ORDER_TYPE,DESCRIPTION,PLANT from order_list group by REVISION");

        if ($query->num_rows() > 0) {
            $no = 1;
            $ret = "";
            foreach ($query->result() as $row) {

                $color = "";
                $ret .= "
            <tr>
                        <td style='background:$color'>$no</td>
            <td>
                            <a class='my-modal' data-target='.mymodal' data-cache='false' data-toggle='modal' data-href= " . base_url() . "index.php/administrator/production_planning/modal_planning/" . $row->REVISION . " href='javascript:void(0);'>$row->REVISION</a>
            </td>
            <td>$row->ORDER_TYPE</td>
                        <td>$row->DESCRIPTION</td>
            <td>$row->PLANT</td>
            <td><a class='btn btn-primary my-modal' data-target='.mymodal' data-cache='false' data-toggle='modal' data-href= " . base_url() . "index.php/administrator/production_planning/modal_planning/" . $row->REVISION . " href='javascript:void(0);'><i class='fa fa-pencil' aria-hidden='true'></i> Set Planning</a></td>
                    </tr>
        ";
                $no++;
            }
            return $ret;
        } else {
            return "";
        }
    }

    function getListArea() {
        $query = $this->db->select('AREA, PHASE')
                ->from("area")
                ->get();
        return $query->result();
    }

    function getProdPlanning($revision) {
        $query = $this->db->select('*')
                ->from("project_planning")
                ->where("REVISION", $revision)
                ->get();
        return $query->result();
    }

    function cekPlanning($revision) {
        $query = $this->db->select('STATUS_PLANNING')
                ->from("order_list")
                ->where("REVISION", $revision)
                ->get();
        return $query->row();
    }

    function getIDPlant($satuan = NULL) {
        $query = $this->db->query("SELECT * from m_plant");
        $ret = "<select id='id_plant' class='form-control' required>"
                . "<option>-- Pilih Plant --</option>";
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                if ($satuan == $row->ID_PLANT) {
                    $slct = "selected";
                } else {
                    $slct = "";
                }
                $ret .= "<option $slct value='$row->PLANT'>$row->PLANT</option>";
            }
        }
        $ret .="</select>";
        return $ret;
    }

    function getIDProject($satuan = NULL) {
        $query = $this->db->query("SELECT * FROM order_list GROUP BY REVISION");
        $ret = "<select id='id_project' class='form-control' required>"
                . "<option>-- Pilih Project --</option>";
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                if ($satuan == $row->REVISION) {
                    $slct = "selected";
                } else {
                    $slct = "";
                }
                $ret .= "<option $slct value='$row->REVISION'>$row->REVISION</option>";
            }
        }
        $ret .="</select>";

        return $ret;
    }


    // model for jobcard perphase dan area
     function getJobcardPhase(){
        $query = $this->db->select('phase')
                ->from('v_jobcard_phase')

                ->get();
        return $query->row();
    }

    function getPhaseInspOpen($parameter){
      $query = $this->db->select('open_')
          ->from('v_jobcard_phase')
          ->where('phase', $parameter)
          ->get();
      return $query->row();
    }

    function getPhaseInspClose($parameter){
      $query = $this->db->select('close_')
          ->from('v_jobcard_phase')
          ->where('phase', $parameter)
          ->get();
      return $query->row();
    }

     function getPhaseInstOpen($parameter){
      $query = $this->db->select('open_')
          ->from('v_jobcard_phase')
          ->where('phase', $parameter)
          ->get();
      return $query->row();
    }

    function getPhaseInstClose($parameter){
      $query = $this->db->select('close_')
          ->from('v_jobcard_phase')
          ->where('phase', $parameter)
          ->get();
      return $query->row();
    }

       function getPhaseOpcOpen($parameter){
      $query = $this->db->select('open_')
          ->from('v_jobcard_phase')
          ->where('phase', $parameter)
          ->get();
      return $query->row();
    }

     function getPhaseOpcClose($parameter){
      $query = $this->db->select('close_')
          ->from('v_jobcard_phase')
          ->where('phase', $parameter)
          ->get();
      return $query->row();
    }

       function getPhaseLubOpen($parameter){
      $query = $this->db->select('open_')
          ->from('v_jobcard_phase')
          ->where('phase', $parameter)
          ->get();
      return $query->row();
    }

    function getPhaseLubClose($parameter){
      $query = $this->db->select('close_')
          ->from('v_jobcard_phase')
          ->where('phase', $parameter)
          ->get();
      return $query->row();
    }

    function getPhaseAFTOpen($parameter){
      $query = $this->db->select('open_')
          ->from('v_jobcard_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

    function getPhaseAFTClose($parameter){
      $query = $this->db->select('close_')
          ->from('v_jobcard_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

    function getPhaseCabinOpen($parameter){
      $query = $this->db->select('open_')
          ->from('v_jobcard_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

    function getPhaseCabinClose($parameter){
      $query = $this->db->select('close_')
          ->from('v_jobcard_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

     function getPhaseELECTOpen($parameter){
      $query = $this->db->select('open_')
          ->from('v_jobcard_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

     function getPhaseELECTClose($parameter){
      $query = $this->db->select('close_')
          ->from('v_jobcard_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

    function getPhaseENG1Open($parameter){
      $query = $this->db->select('open_')
          ->from('v_jobcard_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

    function getPhaseENG1Close($parameter){
      $query = $this->db->select('close_')
          ->from('v_jobcard_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

     function getPhaseFUSELAGEOpen($parameter){
      $query = $this->db->select('open_')
          ->from('v_jobcard_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

    function getPhaseFUSELAGEClose($parameter){
      $query = $this->db->select('close_')
          ->from('v_jobcard_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

    function getPhaseLHWINGOpen($parameter){
      $query = $this->db->select('open_')
          ->from('v_jobcard_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

    function getPhaseLHWINClose($parameter){
      $query = $this->db->select('close_')
          ->from('v_jobcard_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

    //mdr per area
     function getMDRCabinOpen($parameter){
      $query = $this->db->select('open_')
          ->from('v_mdr_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

    function getMDRCabinClose($parameter){
      $query = $this->db->select('close_')
          ->from('v_mdr_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

     function getMDRENG1Open($parameter){
      $query = $this->db->select('open_')
          ->from('v_mdr_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

    function getMDRENG1Close($parameter){
      $query = $this->db->select('close_')
          ->from('v_mdr_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

     function getMDRENG3Open($parameter){
      $query = $this->db->select('open_')
          ->from('v_mdr_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

    function getMDRENG3Close($parameter){
      $query = $this->db->select('close_')
          ->from('v_mdr_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

    function getMDRENG4Open($parameter){
      $query = $this->db->select('open_')
          ->from('v_mdr_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

    function getMDRENG4Close($parameter){
      $query = $this->db->select('close_')
          ->from('v_mdr_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

    function getMDRFUSELAGEOpen($parameter){
      $query = $this->db->select('open_')
          ->from('v_mdr_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

    function getMDRFUSELAGEClose($parameter){
      $query = $this->db->select('close_')
          ->from('v_mdr_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

    function getMDRGENERALOpen($parameter){
      $query = $this->db->select('open_')
          ->from('v_mdr_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

    function getMDRGENERALClose($parameter){
      $query = $this->db->select('close_')
          ->from('v_mdr_area')
          ->where('area', $parameter)
          ->get();
      return $query->row();
    }

    function getJC_AP_Open($parameter){
      $query = $this->db->select('open_')
          ->from('v_jobcard_skill')
          ->where('skill', $parameter)
          ->get();
      return $query->row();
    }

      function getJC_AP_Close($parameter){
      $query = $this->db->select('close_')
          ->from('v_jobcard_skill')
          ->where('skill', $parameter)
          ->get();
      return $query->row();
    }

      function getJC_CBN_Open($parameter){
      $query = $this->db->select('open_')
          ->from('v_jobcard_skill')
          ->where('skill', $parameter)
          ->get();
      return $query->row();
    }

      function getJC_CBN_Close($parameter){
      $query = $this->db->select('close_')
          ->from('v_jobcard_skill')
          ->where('skill', $parameter)
          ->get();
      return $query->row();
    }

    function getJC_EA_Open($parameter){
      $query = $this->db->select('open_')
          ->from('v_jobcard_skill')
          ->where('skill', $parameter)
          ->get();
      return $query->row();
    }

      function getJC_EA_Close($parameter){
      $query = $this->db->select('close_')
          ->from('v_jobcard_skill')
          ->where('skill', $parameter)
          ->get();
      return $query->row();
    }

    function getJC_TBP_Open($parameter){
      $query = $this->db->select('open_')
          ->from('v_jobcard_skill')
          ->where('skill', $parameter)
          ->get();
      return $query->row();
    }

      function getJC_TBP_Close($parameter){
      $query = $this->db->select('close_')
          ->from('v_jobcard_skill')
          ->where('skill', $parameter)
          ->get();
      return $query->row();
    }

    function create(){
    $this->db->insert("customer",array("COMPANY_NAME"=>""));
    return $this->db->insert_id();
  }


  function read(){
    $this->db->order_by("ID_CUSTOMER","desc");
    $query=$this->db->get("customer");
    return $query->result_array();
  }


  function update($id,$value,$modul){
    $this->db->where(array("ID_CUSTOMER"=>$id));
    $this->db->update("customer",array($modul=>$value));
  }

  function delete($ID_CUSTOMER){
    $this->db->where("ID_CUSTOMER",$ID_CUSTOMER);
    $this->db->delete("customer");
  }

  function getJobcard_editgrid($id_project){
        $sql = "select * from jobcard where id_project = '$id_project'";
        return $this->db->query($sql)->result();
 }

  function getTotalJobcard($id_project) {
          $sql = "select * from jobcard where id_project='$id_project'";
          return $this->db->query($sql)->num_rows();
  }

  function getMDR_editgrid($id_project){
        $sql = "select * from mdr where id_project = '$id_project'";
        return $this->db->query($sql)->result();

 }

  function getTotal_MDR($id_project) {
          $sql = "select * from mdr where id_project='$id_project'";
          return $this->db->query($sql)->num_rows();
  }

  public function get_area_jobcard()
        {
            $this->db->order_by('area', 'asc');
            return $this->db->get('jobcard')->result();
        }

  function getMRM_editgrid($id_project){
        $sql = "select * from mrm where id_project = '$id_project'";
        return $this->db->query($sql)->result();
        // $query = $this->db->select('seq, order, description')
        //         ->from("jobcard")
        //         ->get();
        // return $query->result();
  }

   function getTotal_MRM1($id_project) {
        $query = $this->db->select('*')
                ->from('mrm')
                ->where('id_project',$id_project)

                ->get();
        return $query->num_rows();
    }

   function getTotal_MRM($id_project) {
          $sql = "select * from mrm where id_project='$id_project'";
          return $this->db->query($sql)->num_rows();
  }

}

?>
