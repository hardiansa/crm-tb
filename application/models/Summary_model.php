<?php

Class Summary_model extends CI_Model {
   
   function getTotalCreated() {
        $query = $this->db->select('*')
                ->from('project')
                ->where('status_project', "Created")
                // ->where('ID_PROGRESS_STATUS', "1")
                ->get();
        return $query->num_rows();
    }
    function getTotalInProgress() {
        $query = $this->db->select('*')
                ->from('project')
                ->where('status_project', "Released")
                // ->where('ID_PROGRESS_STATUS', "1")
                ->get();
        return $query->num_rows();
    }
    
    function getTotalClose() {
        $query = $this->db->select('*')
                ->from('project')
                ->where('status_project', "Finished")
                // ->where('ID_PROGRESS_STATUS', "2")
                ->get();
        return $query->num_rows();
    }
    

}



?>