<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Prm_model extends Eloquent {

    protected $table = 'prm';
    public $timestamps = false;

}
