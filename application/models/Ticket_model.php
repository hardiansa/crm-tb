<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Illuminate\Database\Eloquent\Model as Eloquent;

class Ticket_model extends Eloquent {

    protected $table = 'ticket';
    public $timestamps = false;

}