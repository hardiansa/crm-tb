<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/dist/img/favicon.ico">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CRM TB</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?php if (isset($is_grocery)): ?>
            <?php foreach ($css_files as $file): ?>
                <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
            <?php endforeach; ?>
            <?php foreach ($js_files as $file): ?>
                <script src="<?php echo $file; ?>"></script>
            <?php endforeach; ?>
        <?php endif; ?>

        <!-- editgrid -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/editablegrid/css/style.css" type="text/css" media="screen">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/editablegrid/css/responsive.css" type="text/css" media="screen">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/editablegrid/css/font-awesome-4.7.0/css/font-awesome.min.css" type="text/css" media="screen">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/editablegrid/css/richtext.min.css" type="text/css" media="screen">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/editablegrid/css/select2.min.css" type="text/css" media="screen">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/custom.css" type="text/css" media="screen">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/jquery-ui/css/smoothness/jquery-ui-1.10.4.custom.min.css" />

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sweetalert/sweetalert.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/jvectormap/jquery-jvectormap.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <!-- Font Awesome -->
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"> -->

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

        <!-- Morris charts -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/morris.js/morris.css">
        <!-- datatables 1. 10 charts -->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <style type="text/css">
            @font-face {
                font-family: lato;
                src: url(<?php echo base_url(); ?>assets/dist/css/lato.ttf);
            }

            body * {
                font-family: lato;
            }

            a.sundaboy:link {color:#FF0000;}

            th {
                white-space: nowrap !important;
            }

            .example-modal .modal {
                position: relative;
                top: auto;
                bottom: auto;
                right: auto;
                left: auto;
                display: block;
                z-index: 1;
            }

            .example-modal .modal {
                background: transparent !important;
            }

            #tablecontent th div {
                resize: horizontal;
                overflow: hidden;
                margin: 0px;
                padding: 2px;
                display: block;
            }

            #tablecontent th {
                margin: 0px;
                padding: 0px;
            }

            #tablecontent td:hover {
                color: black;
            }

            #tablecontent thead tr:hover a {
                color: blue;
            }

            #tablecontent tr:hover {
                color: blue;
                background-color: lightgoldenrodyellow;
            }

            #tablecontent td {
                /* min-width: 100px; */
                max-width: 500px;
                overflow: hidden;
                white-space: nowrap !important;
                /* white-space: normal !important; */
                vertical-align: top;
            }

            .td-close:not(:last-child) {
                background-color: #aeaeae !important;
                font-weight: 900;
                border-bottom: 1px solid #aeaeae !important;
            }

        </style>


        <!-- Custom Style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/custom.css">



        <!-- jQuery 3 -->
        <?php if (!isset($is_grocery)): ?>
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/jquery-1.12.4.min.js" ></script>
            <script src="<?php echo base_url(); ?>assets/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/grocery_crud/texteditor/ckeditor/ckeditor.js"></script>
            <script src="<?php echo base_url(); ?>assets/grocery_crud/texteditor/ckeditor/adapters/jquery.js"></script>
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/jquery.richtext.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/select2.min.js"></script>
            <!-- editgrid js -->
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid.js"></script>
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid_charts.js"></script>
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid_charts_ofc.js"></script>
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid_editors.js"></script>
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid_renderers.js"></script>
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid_utils.js"></script>
            <script src="<?php echo base_url(); ?>assets/editablegrid/js/editablegrid_validators.js"></script>
            <!-- EditableGrid test if jQuery UI is present. If present, a datepicker is automatically used for date type -->
        <?php endif; ?>

        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
        <script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

        <!-- Floating Scroll -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/floating-scroll/src/jquery.floatingscroll.css">
        <script src="<?php echo base_url(); ?>assets/floating-scroll/src/jquery.floatingscroll.js" ></script>

    </head>
    <body class="hold-transition skin-purple-light sidebar-mini sidebar-collapse fixed">
        <div class="wrapper">

            <header class="main-header">

                <!-- Logo -->
                <a href="index.html" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><img src="<?php echo base_url(); ?>assets/dist/img/logokecil.png" width="50"/></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b><img src="<?php echo base_url(); ?>assets/dist/img/logokecil.png" width="50"/></b> <b>CRM</b> TB</span>
                </a>

                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Messages: style can be found in dropdown.less-->

                            <!-- Notifications: style can be found in dropdown.less -->

                            <!-- Tasks: style can be found in dropdown.less -->

                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?php echo $session['name'] ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                        <p><?php echo $session['name'] ?></p>
                                        <p>
                                            <i class="fa fa-user"></i>&nbsp;
                                            <?php if ($session['group_id'] == 1) : ?>
                                                Administrator
                                            <?php elseif ($session['group_id'] == 2) : ?>
                                                Trader
                                            <?php elseif ($session['group_id'] == 3) : ?>
                                                Manager
                                            <?php elseif ($session['group_id'] == 4) : ?>
                                                 Customer
                                            <?php endif; ?>
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?php echo base_url(); ?>login/logout" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo $session['name'] ?></p>
                            <a href="#"><i class="fa fa-user"></i>&nbsp;
                                <?php if ($session['group_id'] == 1) : ?>
                                    Administrator
                                <?php elseif ($session['group_id'] == 2) : ?>
                                    Trader
                                <?php elseif ($session['group_id'] == 3) : ?>
                                    Manager
                                <?php elseif ($session['group_id'] == 4) : ?>
                                    Customer
                                <?php endif; ?></a>
                        </div>
                    </div>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <!--  <li href="#">
                             <a href="<?php echo base_url(); ?>administrator/dashboard">
                                 <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                             </a>

                         </li> -->
                        <li class="#">
                            <a href="<?php echo base_url(); ?>admin/projects/view_vcustomer">
                                <i class="fa fa-address-book"></i> <span>Customer</span>
                            </a>
                        </li>
                        <li class="#">
                            <a href="<?php echo base_url(); ?>admin/projects/view_vproject">
                                <i class="fa fa-th-large"></i> <span>Projects</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#"><i class="fa fa-gears"></i><span>Utility</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="<?php echo base_url() ?>admin/user_management/crud_project_member">
                                        <i class="fa fa-users"></i> <span>Project Member</span></a>
                                </li>
                                <li><a href="<?php echo base_url(); ?>admin/user_management/crud_users">
                                        <i class="fa fa-user"></i> <span>Users</span>
                                    </a></li>
                                <li>
                                    <a href="<?php echo base_url() ?>admin/projects/crud_proposal">
                                        <i class="fa fa-file-text-o"></i> <span>Proposal</span></a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <?php $this->load->view($content); ?>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 2.4.0
                </div>
                <strong>Copyright &copy; 2018.</strong> All rights
                reserved.
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                    <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
                    <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Home tab content -->
                    <div class="tab-pane" id="control-sidebar-home-tab">
                        <h3 class="control-sidebar-heading">Recent Activity</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                        <p>Will be 23 on April 24th</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-user bg-yellow"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                                        <p>New phone +1(800)555-1234</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                                        <p>nora@example.com</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-file-code-o bg-green"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                                        <p>Execution time 5 seconds</p>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->

                        <h3 class="control-sidebar-heading">Tasks Progress</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Custom Template Design
                                        <span class="label label-danger pull-right">70%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Update Resume
                                        <span class="label label-success pull-right">95%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Laravel Integration
                                        <span class="label label-warning pull-right">50%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Back End Framework
                                        <span class="label label-primary pull-right">68%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->

                    </div>
                    <!-- /.tab-pane -->

                    <!-- Settings tab content -->
                    <div class="tab-pane" id="control-sidebar-settings-tab">
                        <form method="post">
                            <h3 class="control-sidebar-heading">General Settings</h3>

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Report panel usage
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Some information about this general settings option
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Allow mail redirect
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Other sets of options are available
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Expose author name in posts
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Allow the user to show his name in blog posts
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <h3 class="control-sidebar-heading">Chat Settings</h3>

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Show me as online
                                    <input type="checkbox" class="pull-right" checked>
                                </label>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Turn off notifications
                                    <input type="checkbox" class="pull-right">
                                </label>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Delete chat history
                                    <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                                </label>
                            </div>
                            <!-- /.form-group -->
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>

        </div>
        <!-- ./wrapper -->
        <script type="text/javascript">
          $(document).ready(function () {
              $(".content").floatingScroll();
          });
        </script>
    </body>
</html>
