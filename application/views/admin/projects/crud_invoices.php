<?php $this->load->view('admin/projects/header'); ?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <?php $this->load->view('admin/projects/menu'); ?>
                <div class="tab-content">
                    <section class="content">
                        <?php echo $output; ?>
                    </section>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
        <!-- /.col -->
    </div>
</section>