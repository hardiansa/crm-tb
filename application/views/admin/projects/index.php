<section class="content-header">
    <h1>
        Project Summary
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Project Summary</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">List Project</h3>
                </div>
                <div class="col-lg-12">
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-6">
                        <form method="post" id="filter-created">
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3><?php echo 0; ?><sup style="font-size: 20px"> </sup></h3>
                                    <p>Created</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-shuffle"></i>
                                </div>
                                <input type="hidden" name="search_text" value="Created" />
                                <input type="hidden" name="search_field" value="status_project" />
                                <a href="javascript:$('#filter-created').submit();" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </form>
                    </div> 
                    <div  class="col-lg-4 col-md-3 col-sm-6 col-xs-6  ">
                        <div class="small-box bg-maroon">
                            <div class="inner">
                                <h3><?php echo 0; ?></h3>

                                <p>In Progress</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-gear-b fa-spin"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-6">
                        <div class="small-box bg-blue">
                            <div class="inner">
                                <h3><?php echo 0; ?><sup style="font-size: 20px"> </sup></h3>

                                <p>Finished</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">                                    
                    <?php echo $output; ?>
                </div>
                <!-- /.box-body -->
            </div>          
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>