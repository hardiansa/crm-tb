<section class="content-header">
    <h1>
        <?php echo $project_name; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="<?php echo base_url(); ?>administrator/projects/index">Project Summary</a></li>
        <li class="active"><?php echo $project_name; ?> </li>
    </ol>
</section>