
<?php $this->load->view('admin/projects/header'); ?>

<section class="content-body">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <?php $this->load->view('admin/projects/menu'); ?>
            </div>
        </div>
    </div>

    <div class="container-full">
        <div class="col-md-12">
            <div class="box box-solid"><?php $this->load->view('admin/projects/title_dashboard'); ?></div>
            
        </div>
    </div>

    <div class="container-full">
        <div class="piechart">
            <div class="row">
                <div class="col-lg-6 col-xs-12">
                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">JOBCARD STATUS</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                </div>
                            </div>

                                <div class="box-body piechart col-md-4">
                                    <div class="sparkline" data-type="pie" data-offset="90" data-width="100px" data-height="100px">
                                        <?php echo $total_jc_close; ?>,<?php echo $total_jc_open; ?>,<?php echo $total_jc_progress; ?>
                                    </div>        
                                </div>

                                <div class="box-body piechart col-md-4">
                                    <ul class="chart-legend clearfix">
                                        <li><i class="fa fa-square text-aqua"></i> Open</li>
                                        <li><i class="fa fa-square text-yellow"></i> Progress</li>
                                        <li><i class="fa fa-square text-green"></i> Close</li>
                                    </ul>
                                </div>

                                <div class="box-body piechart col-md-4">
                                    <ul class="chart-legend clearfix">
                                        <li class="text-aqua"><b><?php echo $percentage_jc_open_pie; ?>%</b></li>
                                        <li class="text-yellow"><b><?php echo $percentage_jc_progress_pie; ?>%</b></li>
                                        <li class="text-green"><b><?php echo $percentage_jc_close_pie; ?>%</b></li>
                                    </ul>
                                </div>

                                <div class="clearfix"></div>
                        </div>
                </div>

                 <div class="col-lg-6 col-xs-12">
                            <div class="box box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">MDR STATUS</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                                    <div class="box-body piechart col-md-4">
                                        <div class="sparkline2" data-type="pie" data-offset="90" data-width="100px" data-height="100px">
                                            <?php echo $total_mdr_close_excel; ?>,<?php echo $total_mdr_open_excel; ?>,<?php echo $total_mdr_progress_excel; ?>
                                        </div>
                                    </div>

                                    <div class="box-body piechart col-md-4">
                                        <ul class="chart-legend clearfix">
                                            <li><i class="fa fa-square text-gray"></i> Open</li>
                                            <li><i class="fa fa-square text-red"></i> Progress</li>
                                            <li><i class="fa fa-square text-purple"></i> Close</li>
                                        </ul>
                                    </div>

                                    <div class="box-body piechart col-md-4">
                                        <ul class="chart-legend clearfix">
                                            <li class="text-aqua"><b><?php echo $percentage_mdr_open_excel_pie; ?>%</b></li>
                                            <li class="text-yellow"><b><?php echo $percentage_mdr_progress_excel_pie; ?>%</b></li>
                                            <li class="text-green"><b><?php echo $percentage_mdr_close_excel_pie; ?>%</b></li>
                                        </ul>
                                    </div>

                                <div class="clearfix"></div>
                        </div>
                </div>
            </div>

            <div class="row">
                  
                        <div class="col-md-6">
                            <div class="container-full">
                                <div class="box box-solid">
                                    <div class="box-header with-border">
                                        <h3 id="jobcard-perphase" class="box-title">JOBCARD PERPHASE</h3>
                                        <div class="chart">
                                            <canvas id="bar-chart_146" width="100" height="50"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="container-full">
                                <div class="box box-solid">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">PROGRESS STATUS JOBCARD PER AREA</h3>
                                        <div class="chart">
                                            <canvas id="bar-chart_143" width="100" height="50"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="box box-solid">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">PROGRESS STATUS MDR PER AREA</h3>
                                        <div class="chart">
                                            <canvas id="bar-chart_144" width="100" height="50"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                        <div class="col-lg-8 col-xs-12">
                            <div class="box box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">MATERIAL STATUS</h3>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <div class="box-body col-md-12 fonts">
                                     <p>NIL STOCK:
                                          <div class="progress">
                                              <div class="progress-bar progress-bar-green progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%;">
                                              50%
                                              </div>
                                          </div>
                                     </p>
                                      <p>SOA:
                                          <div class="progress">
                                              <div class="progress-bar progress-bar-green progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:60%;">
                                              60%
                                             </div>
                                          </div>
                                      </p>
                                      <p>CUST.SPLY:
                                          <div class="progress">
                                              <div class="progress-bar progress-bar-green progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;">
                                              100%
                                              </div>
                                          </div>
                                      </p>
                                      <p>ORDERED PURC:
                                           <div class="progress">
                                              <div class="progress-bar progress-bar-green progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;">
                                              100%
                                              </div>
                                          </div>
                                      </p>
                                      <p>CUSTOMER:
                                             <div class="progress">
                                              <div class="progress-bar progress-bar-green progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;">
                                              100%
                                              </div>
                                          </div>
                                      </p>
                                      <p>SHIPMENT:
                                            <div class="progress">
                                              <div class="progress-bar progress-bar-green progress-bar-striped active" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:78%;">
                                              78%
                                              </div>
                                          </div>
                                      </p>
                                </div>
                               
                                <div class="clearfix"></div>
                                <!-- /.box-body -->
                            </div>
                        </div>

                        <div class="col-lg-4 col-xs-12">
                            <div class="box box-solid">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Close All Area</h3>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>

                                <div class="box-body chart-responsive">
                                    <div style="height: 380px; position: relative;">
                                        <div class="info-box">
                                        <span class="info-box-icon" style="background-color: #0dacc1;"><i class="fa fa-list-alt"></i></span>
                                           <div class="info-box-content fonts">
                                              <span class="info-box-text">Job Card Completion </span>
                                              <span class="info-box-number" style="font-size: 30px"><?php echo $percentage_jc_closed; ?>%</span>
                                              <span class="info-box-text"><?php echo $total_excel_close_jc; ?> of <?php echo $total_excel_jc; ?></span>
                                           </div>
                                        </div>

                                        <div class="info-box">
                                        <span class="info-box-icon" style="background-color: #ffff1a;"><i class="fa fa-list-alt"></i></span>
                                           <div class="info-box-content fonts">
                                              <span class="info-box-text">MDR Completion </span>
                                              <span class="info-box-number" style="font-size: 30px">10%</span>
                                              <span class="info-box-text"><?php echo $total_excel_close_mdr; ?> of  <?php echo $total_excel_mdr; ?></span>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                        </div>
                        <div class="row">
                        
                                <div class="col-md-12">
                                    <div class="container-full">
                                        <div class="box box-solid">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">JOBCARD BY SKILL</h3>
                                             </div>

                                            <div class="chart">
                                                <canvas id="bar-chart_122" width="100" height="50"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end posting area -->
                          
                        <!-- end wrapper -->
                        </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</section>

<script src="<?php echo base_url(); ?>assets/bower_components/chart.js/Chart.min.js"></script>
<script type="text/javascript">
    /// Return with commas in between
    var numberWithCommas = function(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };

    var dataPack1 = [<?php echo $parameter_jobcard_skill_AP_open->open_; ?>, <?php echo $parameter_jobcard_skill_CBN_open->open_; ?>, <?php echo $parameter_jobcard_skill_EA_open->open_; ?>, <?php echo $parameter_jobcard_skill_TBP_open->open_; ?>];
    var dataPack2 = [<?php echo $parameter_jobcard_skill_AP_close->close_; ?>, <?php echo $parameter_jobcard_skill_CBN_close->close_; ?>, <?php echo $parameter_jobcard_skill_EA_close->close_; ?>, <?php echo $parameter_jobcard_skill_TBP_close->close_; ?>];
    var dates = ["A/P", "CBN", "E/A","TBP"];

    // Chart.defaults.global.elements.rectangle.backgroundColor = '#FF0000';

    var bar_ctx = document.getElementById('bar-chart_122');
    var bar_chart = new Chart(bar_ctx, {
        type: 'bar',
        data: {
            labels: dates,
            datasets: [
            {
                label: 'OPEN',
                data: dataPack1,
                backgroundColor: "rgb(63, 191, 63)",
                hoverBackgroundColor: "rgba(55, 160, 225, 0.7)",
                hoverBorderWidth: 2,
                hoverBorderColor: 'lightgrey'
            },
            {
                label: 'CLOSE',
                data: dataPack2,
                backgroundColor: "rgb(191, 63, 63)",
                hoverBackgroundColor: "rgba(225, 58, 55, 0.7)",
                hoverBorderWidth: 2,
                hoverBorderColor: 'lightgrey'
            },
            ]
        },
        options: {
             animation: {
              duration: 10,
            },
            tooltips: {
              mode: 'label',
              callbacks: {
              label: function(tooltipItem, data) { 
                return data.datasets[tooltipItem.datasetIndex].label + ": " + numberWithCommas(tooltipItem.yLabel);
              }
              }
             },
            scales: {
              xAxes: [{ 
                stacked: true, 
                gridLines: { display: false },
                }],
              yAxes: [{ 
                stacked: true, 
                ticks: {
                  callback: function(value) { return numberWithCommas(value); },
                 }, 
                }],
            }, // scales
            legend: {display: true}
        } // options
       }
    );
</script>

<script type="text/javascript">
       // Return with commas in between
    var numberWithCommas = function(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };

    var dataPack3 = [<?php echo $parameter_insp_open->open_; ?>, <?php echo $parameter_inst_open->open_; ?>, <?php echo $parameter_opc_open->open_; ?>, <?php echo $parameter_lub_open->open_; ?>];
    var dataPack4 = [<?php echo $parameter_insp_close->close_; ?>, <?php echo $parameter_inst_close->close_; ?>, <?php echo $parameter_opc_close->close_; ?>, <?php echo $parameter_lub_close->close_; ?>];
    var dates1 = ["INSP", "INST/REST", "OPC/FUC", "SERV/LUB"];

    // Chart.defaults.global.elements.rectangle.backgroundColor = '#FF0000';

    var bar_ctx = document.getElementById('bar-chart_146');
    var bar_chart = new Chart(bar_ctx, {
        type: 'bar',
        data: {
            labels: dates1,
            datasets: [
            {
                label: 'OPEN',
                data: dataPack3,
                backgroundColor: "rgba(55, 160, 225, 0.7)",
                hoverBackgroundColor: "rgba(55, 160, 225, 0.7)",
                hoverBorderWidth: 2,
                hoverBorderColor: 'lightgrey'
            },
            {
                label: 'CLOSE',
                data: dataPack4,
                backgroundColor: "rgba(225, 58, 55, 0.7)",
                hoverBackgroundColor: "rgba(225, 58, 55, 0.7)",
                hoverBorderWidth: 2,
                hoverBorderColor: 'lightgrey'
            },
            ]
        },
        options: {
             animation: {
              duration: 10,
            },
            tooltips: {
              mode: 'label',
              callbacks: {
              label: function(tooltipItem, data) { 
                return data.datasets[tooltipItem.datasetIndex].label + ": " + numberWithCommas(tooltipItem.yLabel);
              }
              }
             },
            scales: {
              xAxes: [{ 
                stacked: true, 
                gridLines: { display: false },
                }],
              yAxes: [{ 
                stacked: true, 
                ticks: {
                  callback: function(value) { return numberWithCommas(value); },
                 }, 
                }],
            }, // scales
            legend: {display: true}
        } // options
       }
    );
</script>

<script type="text/javascript">
    // Return with commas in between
    var numberWithCommas = function(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };

    var dataPack3 = [<?php echo $parameter_aft_open->open_; ?>, <?php echo $parameter_cabin_open->open_; ?>, <?php echo $parameter_elect_open->open_; ?>, <?php echo $parameter_eng1_open->open_; ?>, <?php echo $parameter_fuselage_open->open_; ?>, <?php echo $parameter_lhwing_open->open_; ?>];
    var dataPack4 = [<?php echo $parameter_aft_close->close_; ?>, <?php echo $parameter_cabin_close->close_; ?>, <?php echo $parameter_elect_close->close_; ?>, <?php echo $parameter_eng1_close->close_; ?>, <?php echo $parameter_fuselage_close->close_; ?>, <?php echo $parameter_lhwing_close->close_; ?>];
    var dates1 = ["AFT CARGO", "CABIN", "ELECT", "ENG#1", "FUSELAGE", "LH-WING"];

    // Chart.defaults.global.elements.rectangle.backgroundColor = '#FF0000';

    var bar_ctx = document.getElementById('bar-chart_143');
    var bar_chart = new Chart(bar_ctx, {
        type: 'bar',
        data: {
            labels: dates1,
            datasets: [
            {
                label: 'OPEN',
                data: dataPack3,
                backgroundColor: "rgba(55, 160, 225, 0.7)",
                hoverBackgroundColor: "rgba(55, 160, 225, 0.7)",
                hoverBorderWidth: 2,
                hoverBorderColor: 'lightgrey'
            },
            {
                label: 'CLOSE',
                data: dataPack4,
                backgroundColor: "rgba(225, 58, 55, 0.7)",
                hoverBackgroundColor: "rgba(225, 58, 55, 0.7)",
                hoverBorderWidth: 2,
                hoverBorderColor: 'lightgrey'
            },
            ]
        },
        options: {
             animation: {
              duration: 10,
            },
            tooltips: {
              mode: 'label',
              callbacks: {
              label: function(tooltipItem, data) { 
                return data.datasets[tooltipItem.datasetIndex].label + ": " + numberWithCommas(tooltipItem.yLabel);
              }
              }
             },
            scales: {
              xAxes: [{ 
                stacked: true, 
                gridLines: { display: false },
                }],
              yAxes: [{ 
                stacked: true, 
                ticks: {
                  callback: function(value) { return numberWithCommas(value); },
                 }, 
                }],
            }, // scales
            legend: {display: true}
        } // options
       }
    );
</script>

<script type="text/javascript">
        // Return with commas in between
    var numberWithCommas = function(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };

    var dataPack3 = [<?php echo $parameter_mdr_cabin_open->open_; ?>, <?php echo $parameter_mdr_eng1_open->open_; ?>, <?php echo $parameter_mdr_eng3_open->open_; ?>, <?php echo $parameter_mdr_eng4_open->open_; ?>, <?php echo $parameter_mdr_fuselage_open->open_; ?>, <?php echo $parameter_mdr_general_open->open_; ?>];
    var dataPack4 = [<?php echo $parameter_mdr_cabin_close->close_; ?>, <?php echo $parameter_mdr_eng1_close->close_; ?>, <?php echo $parameter_mdr_eng3_close->close_; ?>, <?php echo $parameter_mdr_eng4_close->close_; ?>, <?php echo $parameter_mdr_fuselage_close->close_; ?>, <?php echo $parameter_mdr_general_close->close_; ?>];
    var dates1 = ["CABIN", "ENG#1", "ENG#3", "ENG#4", "FUSELAGE", "GENERAL AREA"];

    // Chart.defaults.global.elements.rectangle.backgroundColor = '#FF0000';

    var bar_ctx = document.getElementById('bar-chart_144');
    var bar_chart = new Chart(bar_ctx, {
        type: 'bar',
        data: {
            labels: dates1,
            datasets: [
            {
                label: 'OPEN',
                data: dataPack3,
                backgroundColor: "rgba(55, 160, 225, 0.7)",
                hoverBackgroundColor: "rgba(55, 160, 225, 0.7)",
                hoverBorderWidth: 2,
                hoverBorderColor: 'lightgrey'
            },
            {
                label: 'CLOSE',
                data: dataPack4,
                backgroundColor: "rgba(225, 58, 55, 0.7)",
                hoverBackgroundColor: "rgba(225, 58, 55, 0.7)",
                hoverBorderWidth: 2,
                hoverBorderColor: 'lightgrey'
            },
            ]
        },
        options: {
             animation: {
              duration: 10,
            },
            tooltips: {
              mode: 'label',
              callbacks: {
              label: function(tooltipItem, data) { 
                return data.datasets[tooltipItem.datasetIndex].label + ": " + numberWithCommas(tooltipItem.yLabel);
              }
              }
             },
            scales: {
              xAxes: [{ 
                stacked: true, 
                gridLines: { display: false },
                }],
              yAxes: [{ 
                stacked: true, 
                ticks: {
                  callback: function(value) { return numberWithCommas(value); },
                 }, 
                }],
            }, // scales
            legend: {display: true}
        } // options
       }
    );
</script>

<script type="text/javascript">
    $('.sparkline').sparkline('html',
                    {
                        type: 'pie',
                        height: '11.0em',
                        sliceColors: ['#00A65A', '#00c0ef', '#F7BA1C'],
                        highlightLighten: 1.1,
                        tooltipFormat: '{{value}} item - ({{percent.1}}%)',
                    });

            $('.sparkline2').sparkline('html',
                    {
                        type: 'pie',
                        height: '11.0em',
                        sliceColors: ['#605ca8', '#d2d6de', '#f56954'],
                        highlightLighten: 1.1,
                        tooltipFormat: '{{value}} item - ({{percent.1}}%)',
                    });



</script>