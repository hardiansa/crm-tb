<ul class="nav nav-tabs">
    <li>
        <a href="<?php echo base_url(); ?>admin/projects/dashboard/<?php echo $id_project; ?>">Dashboard</a>
    </li>
    <li <?php echo ($content == 'admin/projects/crud_task') ? 'class="active"' : '' ?>>
        <a href="<?php echo base_url(); ?>admin/projects/crud_task/<?php echo $id_project; ?>">Task</a>
    </li>
     <li>
        <a href="#">Timesheets</a>
    </li>
    <li <?php echo ($content == 'admin/projects/crud_jobcard') ? 'class="active"' : '' ?>>
        <a href="<?php echo base_url(); ?>admin/projects/crud_jobcard/<?php echo $id_project; ?>">Jobcard</a>
    </li>
    <li <?php echo ($content == 'admin/projects/crud_mdr') ? 'class="active"' : '' ?>>
        <a href="<?php echo base_url(); ?>admin/projects/crud_mdr/<?php echo $id_project; ?>">MDR</a>
    </li>
    <li <?php echo ($content == 'admin/projects/crud_mrm') ? 'class="active"' : '' ?>>
        <a href="<?php echo base_url(); ?>admin/projects/crud_mrm/<?php echo $id_project; ?>">MRM</a>
    </li>
    <li <?php echo ($content == 'admin/projects/crud_crm') ? 'class="active"' : '' ?>>
        <a href="<?php echo base_url(); ?>admin/projects/crud_crm/<?php echo $id_project; ?>">CRM</a>
    </li>
    <li <?php echo ($content == 'admin/projects/crud_prm') ? 'class="active"' : '' ?>>
        <a href="<?php echo base_url(); ?>admin/projects/crud_prm/<?php echo $id_project; ?>">PRM</a>
    </li>
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            Sales Order <span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
            <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>admin/projects/crud_estimates/<?php echo $id_project; ?>">Estimates</a></li>
            <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url(); ?>admin/projects/crud_invoices/<?php echo $id_project; ?>">Invoices</a></li>
            
        </ul>
    </li>
    <li <?php echo ($content == 'admin/projects/crud_ticket') ? 'class="active"' : '' ?>>
        <a href="<?php echo base_url(); ?>admin/projects/crud_ticket/<?php echo $id_project; ?>">Tickets</a></li>
    <li <?php echo ($content == 'admin/projects/crud_daily_day') ? 'class="active"' : '' ?>>
        <a href="<?php echo base_url(); ?>admin/projects/crud_daily_day/<?php echo $id_project; ?>">Daily</a>
    </li>
    <li <?php echo ($content == 'admin/projects/crud_files') ? 'class="active"' : '' ?>>
        <a href="<?php echo base_url(); ?>admin/projects/crud_files/<?php echo $id_project; ?>">Files</a></li>
    <li><a href="#">Discussions</a></li>
    <li><a href="#">Survey</a></li>
</ul>