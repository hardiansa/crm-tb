<section class="content-header">
    <h1>
        Project Members
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Project Members</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">List Project Members</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">                                    
                    <?php echo $output; ?>
                </div>
                <!-- /.box-body -->
            </div>          
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>