
<style type="text/css">
    p{
        line-height: 0.8;
        margin-top: 15px;
    }
</style>

<div class="row">
    <div class="col-lg-12 col-xs-12">   
        <div class="box box-solid">
            <div class="box">
                <div class="col-lg-12">
                    <h3 class="box-title">Projects Summary</h3>
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-6">
                        <p>
                            <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3><?php echo $total_Created; ?><sup style="font-size: 20px"> </sup></h3>
                                <p>Created</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-shuffle"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                        </p>
                    </div> 
                    <div  class="col-lg-4 col-md-3 col-sm-6 col-xs-6  ">
                        <p>
                        <div class="small-box bg-maroon">
                            <div class="inner">
                                <h3><?php echo $total_InProgress; ?></h3>

                                <p>In Progress</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-gear-b fa-spin"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                        </p>
                    </div>
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-6">
                        <p>
                        <div class="small-box bg-blue">
                            <div class="inner">
                                <h3><?php echo $total_Close; ?><sup style="font-size: 20px"> </sup></h3>

                                <p>Finished</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>          