<section class="content-header">
    <h1>
        Project
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Project Summary</li>
    </ol>
</section>

<!-- Main content -->
<section class="Content-body">
    <!-- Info boxes -->
    <!-- /.row -->
<div class="container-full">
        <div class="col-md-12">
      
            
        </div>
</div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <?php $this->load->view('customer/project_summary/title_summary'); ?>
              
            
                <!-- /.box-header -->
                <div class="box-body">                                    
                 <?php echo $output; ?>
                </div>
                <!-- /.box-body -->
            </div>          
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>