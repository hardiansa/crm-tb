<?php

class User_management extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();

        $this->data["session"] = $this->session->userdata('logged_in');
        if ($this->data["session"]["group_id"] != "1") {
            redirect('index.php/login/logout', 'refresh');
        }
    }

    function index() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'user_management/index';
        $data['listUser'] = $this->user_management_model->getListUser();
        $data['user_management'] = true;
        $this->load->view('template', $data);
    }

    public function crud_users() {
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();
        $crud->set_table('users');
        $crud->set_subject('User');
        $crud->columns('NAME', 'USERNAME', 'GROUP_ID', 'WORK_AREA_ID');
        $crud->fields('NAME', 'USERNAME', 'PASSWORD', 'EMAIL', 'GROUP_ID', 'CUSTOMER_ID', 'last_login', 'STATUS', 'WORK_AREA_ID');
        $crud->change_field_type('last_login', 'invisible');
        $crud->change_field_type('STATUS', 'invisible');
        $crud->display_as('GROUP_ID', 'Access');
        $crud->display_as('WORK_AREA_ID', 'AREA');
        $crud->set_relation('GROUP_ID', 'user_group', 'USER_GROUP');
        $crud->set_relation('WORK_AREA_ID', 'work_area', 'WORK_AREA');
        $crud->set_relation('CUSTOMER_ID', 'customer', 'COMPANY_NAME');
        $crud->field_type('PASSWORD', 'password');
        $crud->required_fields('USERNAME', 'PASSWORD', 'GROUP_ID', 'EMAIL', 'WORK_AREA_ID');
        $crud->callback_before_insert(array($this, 'encrypt_password'));
        $crud->callback_before_update(array($this, 'encrypt_password'));

        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_users';
        $data['user_management'] = true;

        $this->load->view('template', array_merge($data, (array) $output));
    }

     public function crud_project_member() {
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();
        $crud->set_table('project_member');
        $crud->set_subject('');
        $crud->columns('Name','Position','Email','Contact_Person','id_project');
        $crud->fields('Name','Position','Email','Contact_Person','id_project');
        $crud->display_as('id_project', 'Project Name');
        $crud->set_relation('id_project', 'project', 'project_name');
        $crud->required_fields('Name', 'Position', 'Email', 'Contact_Person');

        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/user_management/crud_project_member';
        $data['user_management'] = true;

        $this->load->view('template', array_merge($data, (array) $output));
    }
	

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */

