<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once('assets/editablegrid/EditableGrid.php');

class Projects extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Customer_model');
        $this->load->model('Project_model');
        $this->load->model('VProject_model');
        $this->load->model('VCustomer_model');
        $this->load->model('Jobcard_model');
        $this->load->model('Mdr_model');
        $this->load->model('Mrm_model');
        $this->load->model('Prm_model');
        $this->load->model('Crm_model');
        $this->load->model('Ticket_model');
        $this->load->model('Dashboard_model');

        $this->data["session"] = $this->session->userdata('logged_in');
        if ($this->data["session"]["group_id"] != "1") {
            redirect('login/logout', 'refresh');
        }
    }

    public function index() {
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();
        $crud->set_table('project');
        $crud->set_subject('Project');

        if ($this->uri->segment(4)) {
            if (is_numeric($this->uri->segment(4))) {
                $crud->where('customer_name', $this->uri->segment(4));
                $crud->callback_add_field('customer_name', array($this, 'crud_project_customer_name_add_field_callback'));
            }
        }

        $crud->columns('customer_name', 'aircraft_registered', 'project_name', 'start_date', 'finish_date', 'location', 'status_project', 'type_project', 'Jobcard_Progress', 'MDR_Progress');
        $crud->display_as('customer_name', 'Customer Name');
        $crud->set_relation('customer_name', 'customer', 'COMPANY_NAME');
        $crud->set_rules('finish_date', 'finish_date', 'trim|callback_check_projectdates[' . $this->input->post('start_date') . ']');
        $crud->unset_fields('Id');
        $crud->field_type('location', 'dropdown', array(
            'Hangar 1' => 'Hangar 1',
            'Hangar 3' => 'Hangar 3',
            'Hangar 4' => 'Hangar 4'));
        $crud->field_type('status_project', 'dropdown', array(
            'Not started' => 'Not started',
            'In Progress' => 'In Progress',
            'On Hold' => 'On Hold',
            'Cancelled' => 'Cancelled',
            'Finished' => 'Finished'));
        $crud->field_type('type_project', 'dropdown', array(
            'Retail' => 'Retail',
            'Contract' => 'Contract'));
        $crud->unset_add();
        $crud->unset_read();
        $crud->unset_delete();
        $crud->unset_edit();
        $crud->required_fields('location', 'status_project', 'type_project');
        $crud->callback_column('project_name', array($this, 'callback_project'));
        $crud->callback_column('Jobcard_Progress', array($this, 'callback_jobcard_progress'));
        $crud->callback_column('MDR_Progress', array($this, 'callback_mdr_progress'));
        $crud->add_action('Detail', base_url() . 'assets/dist/img/icons_dashboard.png', 'admin/projects/crud_jobcard'); // , 'admin/projects/project_tabs');
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/index';

        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function callback_project($value, $row) {

        return '<a href="' . base_url() . 'admin/projects/crud_jobcard/' . $row->Id . '">' . $value . '</a>';
    }

    public function callback_jobcard_progress($value, $row) {
        $close = $this->Jobcard_model->where('id_project', '=', $row->Id)
                ->where('status', 'like', 'Close')
                ->count();
        $all = $this->Jobcard_model->where('id_project', '=', $row->Id)->count();
        $persen = round(($close / ($all ? $all : 1)) * 10000) / 100;
        return '<a href="' . base_url() . 'admin/projects/crud_jobcard/' . $row->Id . '">' . $persen . '%</a>';
    }

    public function callback_mdr_progress($value, $row) {
        $close = $this->Mdr_model->where('id_project', '=', $row->Id)
                ->where('status', 'like', 'Close')
                ->count();
        $all = $this->Mdr_model->where('id_project', '=', $row->Id)->count();
        $persen = round(($close / ($all ? $all : 1)) * 10000) / 100;
        return '<a href="' . base_url() . 'admin/projects/crud_mdr/' . $row->Id . '">' . $persen . '%</a>';
    }

    public function check_projectdates($finish_date, $start_date) {
        $parts = explode('/', $this->input->post('start_date'));
        $start_date = join('-', $parts);
        $start_date = strtotime($start_date);

        $parts2 = explode('/', $this->input->post('finish_date'));
        $finish_date = join('-', $parts2);
        $finish_date = strtotime($finish_date);

        if ($finish_date >= $start_date) {

            return true;
        } else {
            $this->form_validation->set_message('check_projectdates', "finish date should be greater than start date");
            return false;
        }
    }

    public function crud_project_customer_name_add_field_callback() {
        return $this->Customer_model->where('ID_CUSTOMER', $this->uri->segment(4))->first()->COMPANY_NAME . '<input type="hidden" name="customer_name" value="' . $this->uri->segment(4) . '" />';
    }

    public function crud_customer() {
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();
        $crud->set_table('customer');
        $crud->set_subject('Customer');
        $crud->unset_fields('ID_CUSTOMER');
        $crud->unset_columns('ID_CUSTOMER');
        $crud->unset_add();
        $crud->unset_read();
        $crud->unset_delete();
        $crud->unset_edit();
        $crud->add_action('Project', base_url() . 'assets/dist/img/icons_dashboard.png', 'admin/projects/index');
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_customer';

        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function crud_users() {
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();
        $crud->set_table('users');
        $crud->set_subject('User');
        $crud->columns('NAME', 'USERNAME', 'GROUP_ID', 'WORK_AREA_ID');
        $crud->fields('NAME', 'USERNAME', 'PASSWORD', 'EMAIL', 'GROUP_ID', 'CUSTOMER_ID', 'last_login', 'STATUS', 'WORK_AREA_ID');
        $crud->change_field_type('last_login', 'invisible');
        $crud->change_field_type('STATUS', 'invisible');
        $crud->display_as('GROUP_ID', 'Access');
        $crud->display_as('WORK_AREA_ID', 'AREA');
        $crud->set_relation('GROUP_ID', 'user_group', 'USER_GROUP');
        $crud->set_relation('WORK_AREA_ID', 'work_area', 'WORK_AREA');
        $crud->set_relation('CUSTOMER_ID', 'customer', 'COMPANY_NAME');
        $crud->field_type('PASSWORD', 'password');
        $crud->required_fields('USERNAME', 'PASSWORD', 'GROUP_ID', 'EMAIL', 'WORK_AREA_ID');
        $crud->callback_before_insert(array($this, 'encrypt_password'));
        $crud->callback_before_update(array($this, 'encrypt_password'));

        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_users';
        $data['projects'] = true;

        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function encrypt_password($post_array) {

        $this->load->helper('security');
        $post_array['PASSWORD'] = do_hash($post_array['PASSWORD'], 'md5');
        $post_array['STATUS'] = 1;
        return $post_array;
    }

    public function crud_daily_day() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data['is_grocery'] = true;

        $crud = new grocery_CRUD();

        $crud->set_table('daily_day');
        $crud->where('id_project', $this->session->userdata('id_project'));
        $crud->set_subject('Daily');
        // $crud->add_action('Detail', '', '',base_url().'assets/dist/img/icons_dashboard.png',array($this,'just_a_test'));
        $crud->columns('area', 'task_progress', 'followup_result', 'remarks', 'create_date_daily');
        $crud->field_type('area', 'dropdown', array('ENGINE' => 'ENGINE', 'WING & FLT CONTROL' => 'WING & FLT CONTROL', 'FWD & AFT CARGO' => 'FWD & AFT CARGO', 'LDG/HYD' => 'LDG/HYD', 'TAIL & APU' => 'TAIL & APU', 'AVIONIC' => 'AVIONIC', 'CABIN' => 'CABIN', 'MAIN DECK CARGO' => 'MAIN DECK CARGO', 'CABIN SHOP' => 'CABIN SHOP', 'STRUCTURE/FUSELAGE' => 'STRUCTURE/FUSELAGE', 'OTHERS' => 'OTHERS'));
        $crud->unset_fields('create_date_daily', 'update_date_daily', 'createdBy_daily', 'updateBy_daily', 'id_project');
        $crud->callback_after_insert(array($this, 'daily_day_after_insert'));
        $crud->callback_after_update(array($this, 'daily_day_after_update'));
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_daily_day';

        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function daily_day_after_insert($post_array, $primary_key) {
        $daily_day_update = array(
            "create_date_daily" => date('Y-m-d H:i:s'),
            "update_date_daily" => date('Y-m-d H:i:s'),
            "id_project" => $this->session->userdata('id_project'),
            "createdBy_daily" => $this->session->userdata('logged_in')['id_user'],
            "updateBy_daily" => $this->session->userdata('logged_in')['id_user']
        );

        $this->db->update('daily_day', $daily_day_update, array('id' => $primary_key));

        return true;
    }

    public function daily_day_after_update($post_array, $primary_key) {
        $daily_day_update = array(
            "update_date_daily" => date('Y-m-d H:i:s'),
            "updateBy_daily" => $this->session->userdata('logged_in')['id_user']
        );

        $this->db->update('daily_day', $daily_day_update, array('id' => $primary_key));

        return true;
    }

    public function view_vcustomer() {
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/view_vcustomer';

        $this->load->view('template', $data);
    }

    public function view_vcustomer_load() {
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

//        $grid->addColumn('ID_CUSTOMER', 'ID_CUSTOMER', 'string', NULL, false);
        $grid->addColumn('COMPANY_NAME', 'COMPANY_NAME', 'string', NULL, false);
        $grid->addColumn('COMPANY_EMAIL', 'COMPANY_EMAIL', 'string', NULL, false);
        $grid->addColumn('COMPANY_ADDRESS', 'COMPANY_ADDRESS', 'string', NULL, false);
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'ID_CUSTOMER');

        $active_vcustomer = $this->VCustomer_model;

        $totalUnfiltered = $active_vcustomer->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];

        $rowByPage = 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_vcustomer = $active_vcustomer
                    ->where('project_name', 'like', '%' . $filter . '%');
            $total = $active_vcustomer->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "" && in_array($_GET['sort'], $grid->getColumnFields())) {
            $$active_vcustomer = $active_vcustomer->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_vcustomer = $active_vcustomer->orderBy('COMPANY_NAME', 'ASC');
        }

        $active_vcustomer = $active_vcustomer->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);

        $grid->renderJSON($active_vcustomer->get(), false, false, !isset($_GET['data_only']));
    }

    public function view_vproject() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_customer', $this->uri->segment(4));
            } else {
                $this->session->unset_userdata('id_customer');
            }
            $data['id_customer'] = $this->session->userdata('id_customer');
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/view_vproject';

        $this->load->view('template', $data);
    }

    public function view_vproject_load() {
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $customer_array = array();
        $customers = $this->Customer_model->all();
        foreach ($customers as $customer) {
            $customer_array[$customer->ID_CUSTOMER] = $customer->COMPANY_NAME;
        }

        $grid->addColumn('customer_name', 'Customer Name', 'string', $customer_array, false);
        $grid->addColumn('aircraft_registered', 'Aircraft Registered', 'string', NULL, false);
        $grid->addColumn('project_name', 'Project Name', 'string', NULL, false);
        $grid->addColumn('start_date', 'Start Date', 'string', NULL, false);
        $grid->addColumn('finish_date', 'Finish Date', 'string', NULL, false);
        $grid->addColumn('location', 'Location', 'string', NULL, false);
        $grid->addColumn('status_project', 'Status Project', 'string', NULL, false);
        $grid->addColumn('type_project', 'Type Project', 'string', NULL, false);
//        $grid->addColumn('Jobcard_Progress', 'Jobcard Progress', 'string', NULL, false);
//        $grid->addColumn('MDR_Progress', 'MDR Progress', 'string', NULL, false);
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'Id');

        $active_vproject = $this->VProject_model;
        if ($this->session->userdata('id_customer') != NULL) {
            $active_vproject = $active_vproject->where('customer_name', $this->session->userdata('id_customer'));
        }

        $totalUnfiltered = $active_vproject->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];

        $rowByPage = 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_vproject = $active_vproject
                    ->where('project_name', 'like', '%' . $filter . '%');
            $total = $active_vproject->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "" && in_array($_GET['sort'], $grid->getColumnFields())) {
            $$active_vproject = $active_vproject->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_vproject = $active_vproject->orderBy('customer_name', 'ASC');
        }

        $active_vproject = $active_vproject->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);

        $grid->renderJSON($active_vproject->get(), false, false, !isset($_GET['data_only']));
    }

    public function crud_jobcard() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_jobcard';

        $this->load->view('template', $data);
    }

    public function crud_jobcard_load() {
        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $grid->addColumn('seq', 'Seq', 'integer');
        $grid->addColumn('jc_order', 'Order', 'string', NULL, false);
        $grid->addColumn('description', 'Description', 'html', NULL, false);
        $grid->addColumn('cus_jc_num_real', 'Cust JC Num', 'string', NULL, false);
        $grid->addColumn('task', 'Task', 'string', NULL, false);
        $grid->addColumn('itval_real', 'ITVAL', 'string', NULL, false);
        $grid->addColumn('mhrs_plan', 'Mhrs Plan', 'float', NULL, false);
        $grid->addColumn('skill', 'Skill', 'string', array("A/P" => "A/P", "CBN" => "CBN", "E/A" => "E/A", "NDT" => "NDT", "STR" => "STR", "TBP" => "TBP"));
        $grid->addColumn('area', 'Area', 'string', array("FUSELAGE" => "FUSELAGE", "COCKPIT" => "COCKPIT", "LH-WING" => "LH-WING", "RH-WING" => "RH-WING", "L/G" => "L/G", "ENG#1" => "ENG#1", "ENG#2" => "ENG#2", "ENG#3" => "ENG#3", "ENG#4" => "ENG#4", "TAIL" => "TAIL", "CABIN" => "CABIN", "FWD CARGO" => "FWD CARGO", "AFT CARGO" => "AFT CARGO", "BULK CARGO" => "BULK CARGO", "MAIN CARGO" => "MAIN CARGO", "LOW CARGO" => "LOW CARGO", "ELECT" => "ELECT", "GENERAL AREA" => "GENERAL AREA"));
        $grid->addColumn('phase', 'Phase', 'string', array("INSP" => "INSP", "PRELIM" => "PRELIM", "OPN/REM" => "OPN/REM", "REP-RECT" => "REP-RECT", "SERV/LUB" => "SERV/LUB", "INST/REST" => "INST/REST", "OPC/FUC" => "OPC/FUC", "GRND/CREW ACPT" => "GRND/CREW ACPT", "FINISH" => "FINISH", "REDEV" => "REDEV"));
        $grid->addColumn('day', 'Day', 'integer');
        $grid->addColumn('date_close', 'Date Closed', 'date');
        $grid->addColumn('jc_reff_mdr_real', 'JC Reff MDR', 'integer', NULL, false);
        $grid->addColumn('remark', 'Remark', 'string', array("SENT TO CABIN" => "SENT TO CABIN", "SENT TO CABIN SHOP" => "SENT TO CABIN SHOP", "SENT TO PAINTING" => "SENT TO PAINTING", "SENT TO NDT" => "SENT TO NDT", "SENT TO TBR SHOP" => "SENT TO TBR SHOP", "SENT TO SEALANT" => "SENT TO SEALANT", "SENT TO STR HANGAR" => "SENT TO STR HANGAR", "SENT TO SEAT" => "SENT TO SEAT", "SENT TO WHEEL SHOP" => "SENT TO WHEEL SHOP", "SENT TO TV ENGINE" => "SENT TO TV ENGINE", "SENT TO CLEANING" => "SENT TO CLEANING", "N/A (NOT APPLICABLE)" => "N/A (NOT APPLICABLE)", "WITHDRAWN" => "WITHDRAWN", "COVER BY ANOTHER JOBCARD" => "COVER BY ANOTHER JOBCARD", "INTERUPT" => "INTERUPT", "MAP SHORTAGE" => "MAP SHORTAGE", "MAP GADC" => "MAP GADC", "MAP W. CUST. SUPPLY" => "MAP W. CUST. SUPPLY", "PART COMPLETE" => "PART COMPLETE", "PART PARTIAL" => "PART PARTIAL", "ANOTHER REASON" => "ANOTHER REASON"));
        $grid->addColumn('free_text', 'Free Text', 'html');
        $grid->addColumn('date_progress', 'Date Progress', 'date');
        $grid->addColumn('mat_status', 'Mat Status', 'string', NULL, false);
        $grid->addColumn('cabin_status', 'Cabin Status', 'string', NULL, false);
        $grid->addColumn('status', 'Status', 'string', array("Open" => "Open", "Close" => "Close", "Perform by Prod" => "Perform by Prod", "Perform To Shop" => "Perform To Shop", "Waiting Material" => "Waiting Material", "Waiting Tool" => "Waiting Tool", "Prepare for Install" => "Prepare for Install", "Prepare for Test" => "Prepare for Test", "Prepare for Run Up" => "Prepare for Run Up", "Prepare for NDT" => "Prepare for NDT", "Part Avail" => "Part Avail"));
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'id');

        $active_job = $this->Jobcard_model
                ->selectRaw('*,'
                        . '(SELECT COUNT(*) FROM mdr WHERE mdr.jc_reff = jobcard.id) AS jc_reff_mdr_real,'
                        . 'SUBSTRING(jobcard.description, '
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "CHARINDEX('[',jobcard.description)" : "LOCATE('[',jobcard.description)") . '+1, ABS('
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "CHARINDEX(']',jobcard.description)" : "LOCATE(']',jobcard.description)") . '-'
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "CHARINDEX('[',jobcard.description)" : "LOCATE('[',jobcard.description)") . '-1)) AS itval_real,'
                        . 'SUBSTRING(jobcard.description, '
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "CHARINDEX(']',jobcard.description)" : "LOCATE(']',jobcard.description)") . '+1, ABS('
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "CHARINDEX(':',jobcard.description)" : "LOCATE(':',jobcard.description)") . '-'
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "CHARINDEX(']',jobcard.description)" : "LOCATE(']',jobcard.description)") . '-1)) AS cus_jc_num_real')
                ->where('id_project', $this->session->userdata('id_project'));

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];


        $rowByPage = $totalUnfiltered; // 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where('jc_order', 'like', '%' . $filter . '%')
                    ->orwhere('description', 'like', '%' . $filter . '%')
                    ->orwhere('cus_jc_num', 'like', '%' . $filter . '%')
                    ->orwhere('task', 'like', '%' . $filter . '%')
                    ->orwhere('itval', 'like', '%' . $filter . '%')
                    ->orwhere('mhrs_plan', 'like', '%' . $filter . '%')
                    ->orwhere('skill', 'like', '%' . $filter . '%')
                    ->orwhere('area', 'like', '%' . $filter . '%')
                    ->orwhere('Phase', 'like', '%' . $filter . '%')
                    ->orwhere('day', 'like', '%' . $filter . '%')
                    ->orwhere('status', 'like', '%' . $filter . '%')
                    ->orwhere('date_close', 'like', '%' . $filter . '%')
                    /* ->orwhere('jc_reff_mdr', 'like', '%' . $filter . '%') */
                    ->orwhere('remark', 'like', '%' . $filter . '%')
                    ->orwhere('free_text', 'like', '%' . $filter . '%')
                    ->orwhere('date_progress', 'like', '%' . $filter . '%')
                    ->orwhere('mat_status', 'like', '%' . $filter . '%')
                    ->orWhere('cabin_status', 'like', '%' . $filter . '%');
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "" && in_array($_GET['sort'], $grid->getColumnFields())) {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('seq', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);

        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function crud_jobcard_add() {
        $crud = new grocery_CRUD();

        $crud->set_table('jobcard');
        $crud->set_subject('Jobcard');
        $crud->columns('seq', 'skill', 'area', 'phase', 'day', 'status', 'date_close', 'remark', 'free_text', 'date_progress');
        $crud->field_type('skill', 'dropdown', array("A/P" => "A/P", "CBN" => "CBN", "E/A" => "E/A"
            , "NDT" => "NDT", "STR" => "STR", "TBP" => "TBP"));
        $crud->field_type('area', 'dropdown', array('FUSELAGE' => 'FUSELAGE', 'COCKPIT' => 'COCKPIT', 'LH-WING' => 'LH-WING', 'RH-WING' => 'RH-WING', 'L/G' => 'L/G', 'ENG#1' => 'ENG#1', 'ENG#2' => 'ENG#2', 'ENG#3' => 'ENG#3', 'ENG#4' => 'ENG#4', 'TAIL' => 'TAIL', "CABIN" => "CABIN", "FWD CARGO" => "FWD CARGO", "AFT CARGO" => "AFT CARGO", "BULK CARGO" => "BULK CARGO", "MAIN CARGO" => "MAIN CARGO", "LOW CARGO" => "LOW CARGO", "ELECT" => "ELECT", "GENERAL AREA" => "GENERAL AREA"));
        $crud->field_type('phase', 'dropdown', array("INSP" => "INSP", "PRELIM" => "PRELIM", "OPN/REM" => "OPN/REM", "REP-RECT" => "REP-RECT", "SERV/LUB" => "SERV/LUB", "INST/REST" => "INST/REST", "OPC/FUC" => "OPC/FUC", "GRND/CREW ACPT" => "GRND/CREW ACPT", "FINISH" => "FINISH", "REDEV" => "REDEV"));
        $crud->field_type('status', 'dropdown', array("Open" => "Open", "Close" => "Close", "Perform by Prod" => "Perform by Prod", "Perform To Shop" => "Perform To Shop", "Waiting Material" => "Waiting Material", "Waiting Tool" => "Waiting Tool", "Prepare for Install" => "Prepare for Install", "Prepare for Test" => "Prepare for Test", "Prepare for Run Up" => "Prepare for Run Up", "Prepare for NDT" => "Prepare for NDT", "Part Avail" => "Part Avail"));
        $crud->field_type('remark', 'dropdown', array("SENT TO CABIN" => "SENT TO CABIN", "SENT TO CABIN SHOP" => "SENT TO CABIN SHOP", "SENT TO PAINTING" => "SENT TO PAINTING", "SENT TO NDT" => "SENT TO NDT", "SENT TO TBR SHOP" => "SENT TO TBR SHOP", "SENT TO SEALANT" => "SENT TO SEALANT", "SENT TO STR HANGAR" => "SENT TO STR HANGAR", "SENT TO SEAT" => "SENT TO SEAT", "SENT TO WHEEL SHOP" => "SENT TO WHEEL SHOP", "SENT TO TV ENGINE" => "SENT TO TV ENGINE", "SENT TO CLEANING" => "SENT TO CLEANING", "N/A (NOT APPLICABLE)" => "N/A (NOT APPLICABLE)", "WITHDRAWN" => "WITHDRAWN", "COVER BY ANOTHER JOBCARD" => "COVER BY ANOTHER JOBCARD", "INTERUPT" => "INTERUPT", "MAP SHORTAGE" => "MAP SHORTAGE", "MAP GADC" => "MAP GADC", "MAP W. CUST. SUPPLY" => "MAP W. CUST. SUPPLY", "PART COMPLETE" => "PART COMPLETE", "PART PARTIAL" => "PART PARTIAL", "ANOTHER REASON" => "ANOTHER REASON"));
        $crud->unset_fields('task', 'itval', 'mhrs_plan', 'description', 'cus_jc_num', 'jc_order', 'mhrs_plan', 'task', 'id_project', 'id', 'date_created', 'jc_reff_mdr', 'mat_status', 'status_sap', 'mat_status', 'cabin_status');
        $crud->callback_after_insert(array($this, 'crud_jobcard_after_insert'));
        $crud->callback_add_field('seq', array($this, 'crud_jobcard_seq_add_field_callback'));
        $crud->unset_back_to_list();
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');

        $this->load->view('example', array_merge($data, (array) $output));
    }

    function crud_jobcard_after_insert($post_array, $primary_key) {
        $jobcard_update = array(
            "id_project" => $this->session->userdata('id_project')
        );
        $this->db->update('jobcard', $jobcard_update, array('id' => $primary_key));
        return true;
    }

    function crud_jobcard_seq_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_seq = 0;
        try {
            $query = $this->Jobcard_model
                            ->where('id_project', $this->session->userdata('id_project'))
                            ->orderBy('seq', 'DESC')
                            ->first()->seq;
            // $last = $query->seq;
            $last_seq = $query + 1;
            error_log($this->session->userdata('id_project'));
        } catch (Exception $e) {
            error_log($e);
            $last_seq = 1;
        }
        return '<input type="text" value="' . $last_seq . '" name="seq">';
    }

    public function crud_jobcard_update() {
        $data["session"] = $this->session->userdata('logged_in');

        $colname = strip_tags($_POST['colname']);
        $id = strip_tags($_POST['id']);
        $coltype = strip_tags($_POST['coltype']);
        $value = ($coltype == 'html') ? $_POST['newvalue'] : strip_tags($_POST['newvalue']);
        // $tablename = strip_tags($_POST['tablename']);

        if ($coltype == 'date') {
            if ($value === "") {
                $value = NULL;
            } else {
                $date_info = date_parse_from_format('d/m/Y', $value);
                $value = "{$date_info['year']}-{$date_info['month']}-{$date_info['day']}";
            }
        }

        try {
            $this->Jobcard_model->where('id', $id)->update([$colname => $value]);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_jobcard_delete() {
        $data["session"] = $this->session->userdata('logged_in');

        $id = strip_tags($_POST['id']);
        // $tablename = strip_tags($_POST['tablename']);

        try {
            $this->Jobcard_model->destroy($id);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_mdr() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_mdr';

        $this->load->view('template', $data);
    }

    public function crud_mdr_load() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $jc_array = array();
        $jc_orders = $this->Jobcard_model->where('id_project', $data['id_project'])->get();
        foreach ($jc_orders as $jc_order) {
            $jc_array[$jc_order->id] = $jc_order->jc_order;
        }

        $grid->addColumn('seq', 'Seq', 'integer');
        $grid->addColumn('mdr_order', 'MDR Order', 'string', NULL, false);
        $grid->addColumn('jc_reff', 'JC REFF', 'string', $jc_array);
        $grid->addColumn('ori_taskcard_real', 'Ori. Taskcard', 'string', NULL, false);
        $grid->addColumn('discrepancies', 'Discrepancies', 'html', NULL, false);
        $grid->addColumn('area_code', 'AREA Code', 'string', NULL, false);
        $grid->addColumn('created_on', 'Created On', 'string', NULL, false);
        $grid->addColumn('main_skill', 'Main Skill', 'string', NULL, false);
        $grid->addColumn('iss_by', 'Iss. By', 'string', NULL, false);
        $grid->addColumn('date_from_pe', 'Date from PE', 'date');
        $grid->addColumn('accomp_status', 'Accomp. Status', 'string', array("Carry Out" => "Carry Out", "Perform by Prod" => "Perform by Prod", "Perform To Shop" => "Perform To Shop", "Waiting Material" => "Waiting Material", "Waiting Tool" => "Waiting Tool", "Next RO" => "Next RO", "Waiting RO" => "Waiting RO", "Waiting Deployment" => "Waiting Deployment", "Waiting Cust Approval" => "Waiting Cust Approval", "Prepare for Test" => "Prepare for Test", "Prepare for Run Up" => "Prepare for Run Up", "Prepare for NDT" => "Prepare for NDT", "Part Avail" => "Part Avail"));
        $grid->addColumn('mat_status', 'Mat Status', 'string', array("Shortage" => "Shortage", "GADC" => "GADC", "WCS" => "WCS"));
        $grid->addColumn('date', 'Date', 'date');
        $grid->addColumn('step1', 'STEP1', 'string', array("A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
        $grid->addColumn('date1', 'Date 1 ', 'date');
        $grid->addColumn('step2', 'STEP2', 'string', array("A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
        $grid->addColumn('date2', 'Date 2', 'date');
        $grid->addColumn('step3', 'STEP3', 'string', array("A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
        $grid->addColumn('date3', 'Date 3', 'date');
        $grid->addColumn('date_close', 'Date Close', 'date');
        $grid->addColumn('material_status_mrm', 'Material Status MRM', 'string', NULL, false);
        $grid->addColumn('remark', 'Remark', 'string', array("SENT TO CABIN" => "SENT TO CABIN", "SENT TO CABIN SHOP" => "SENT TO CABIN SHOP", "SENT TO PAINTING" => "SENT TO PAINTING", "SENT TO NDT" => "SENT TO NDT", "SENT TO TBR SHOP" => "SENT TO TBR SHOP", "SENT TO SEALANT" => "SENT TO SEALANT", "SENT TO STR HANGAR" => "SENT TO STR HANGAR", "SENT TO SEAT" => "SENT TO SEAT", "SENT TO WHEEL SHOP" => "SENT TO WHEEL SHOP", "SENT TO TV ENGINE" => "SENT TO TV ENGINE", "SENT TO CLEANING" => "SENT TO CLEANING", "MDR BELUM KE PPC" => "MDR BELUM KE PPC", "MATERIAL" => "MATERIAL", "RO BY ->" => "RO BY ->", "WAITING JOB ->" => "WAITING JOB", "COVER BY" => "COVER BY", "INTERUPT" => "INTERUPT", "MAP SHORTAGE" => "MAP SHORTAGE", "MAP GADC" => "MAP GADC", "MAP W. CUST. SUPPLY" => "MAP W. CUST. SUPPLY", "PART COMPLETE" => "PART COMPLETE", "PART PARTIAL" => "PART PARTIAL", "ANOTHER REASON" => "ANOTHER REASON"));
        $grid->addColumn('free_text', 'Free Text', 'string');
        $grid->addColumn('date_progress', 'Date Progress', 'date');
        $grid->addColumn('day', 'Day', 'integer');
        $grid->addColumn('cabin_status', 'Cabin Status', 'string', NULL, false);
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'id');

        $active_mdr = $this->Mdr_model
                ->selectRaw('*,'
                        . '(SELECT '
                        // . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? " TOP 1 " : "")
                        . ' SUBSTRING(jobcard.description, '
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "CHARINDEX(']',jobcard.description)" : "LOCATE(']',jobcard.description)") . '+1, ABS('
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "CHARINDEX(':',jobcard.description)" : "LOCATE(':',jobcard.description)") . '-'
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "CHARINDEX(']',jobcard.description)" : "LOCATE(']',jobcard.description)") . '-1)) FROM jobcard WHERE (jobcard.id = mdr.jc_reff) OR (jobcard.jc_order = mdr.jc_reff) '
                        . ((($this->db->dbdriver == 'sqlsrv') or ( $this->db->dbdriver == 'mssql')) ? "" : " LIMIT 1 ")
                        . ' ) AS ori_taskcard_real')
                ->where('id_project', $data['id_project']);

        $totalUnfiltered = $active_mdr->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];


        $rowByPage = $totalUnfiltered; // 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_mdr = $active_mdr
                    ->where('mdr_order', 'like', '%' . $filter . '%')
                    ->orwhere('jc_reff', 'like', '%' . $filter . '%')
                    ->orwhere('ori_taskcard', 'like', '%' . $filter . '%')
                    ->orwhere('discrepancies', 'like', '%' . $filter . '%')
                    ->orwhere('area_code', 'like', '%' . $filter . '%')
                    ->orwhere('main_skill', 'like', '%' . $filter . '%')
                    ->orwhere('iss_by', 'like', '%' . $filter . '%')
                    ->orwhere('date_from_pe', 'like', '%' . $filter . '%')
                    ->orwhere('accomp_status', 'like', '%' . $filter . '%')
                    ->orwhere('mat_status', 'like', '%' . $filter . '%')
                    ->orwhere('date', 'like', '%' . $filter . '%')
                    ->orwhere('step1', 'like', '%' . $filter . '%')
                    ->orwhere('date1', 'like', '%' . $filter . '%')
                    ->orwhere('step2', 'like', '%' . $filter . '%')
                    ->orwhere('date2', 'like', '%' . $filter . '%')
                    ->orwhere('step3', 'like', '%' . $filter . '%')
                    ->orwhere('date3', 'like', '%' . $filter . '%')
                    ->orwhere('status', 'like', '%' . $filter . '%')
                    ->orwhere('date_close', 'like', '%' . $filter . '%')
                    ->orwhere('material_status_mrm', 'like', '%' . $filter . '%')
                    ->orwhere('remark', 'like', '%' . $filter . '%')
                    ->orwhere('free_text', 'like', '%' . $filter . '%')
                    ->orwhere('date_progress', 'like', '%' . $filter . '%')
                    ->orwhere('day', 'like', '%' . $filter . '%')
                    ->orwhere('cabin_status', 'like', '%' . $filter . '%');
            $total = $active_mdr->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "" && in_array($_GET['sort'], $grid->getColumnFields())) {
            $active_mdr = $active_mdr->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_mdr = $active_mdr->orderBy('seq', 'ASC');
        }

        $active_mdr = $active_mdr->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);

        $grid->renderJSON($active_mdr->get(), false, false, !isset($_GET['data_only']));
    }

    public function crud_mdr_add() {
        $crud = new grocery_CRUD();

        $crud->set_table('mdr');
        $crud->set_subject('Mdr');
        $crud->columns('seq', 'date_from_pe', 'accomp_status', 'mat_status', 'date', 'step1', 'date1', 'step2', 'date2', 'step3', 'date3', 'status_mdr', 'date_close', 'remark', 'free_text', 'date_progress', 'day');
        $crud->field_type('accomp_status', 'dropdown', array("Carry Out" => "Carry Out", "Perform by Prod" => "Perform by Prod", "Perform To Shop" => "Perform To Shop", "Waiting Material" => "Waiting Material", "Waiting Tool" => "Waiting Tool", "Next RO" => "Next RO", "Waiting RO" => "Waiting RO", "Waiting Deployment" => "Waiting Deployment", "Waiting Cust Approval" => "Waiting Cust Approval", "Prepare for Test" => "Prepare for Test", "Prepare for Run Up" => "Prepare for Run Up", "Prepare for NDT" => "Prepare for NDT", "Part Avail" => "Part Avail"));
        $crud->field_type('mat_status', 'dropdown', array("Shortage" => "Shortage", "GADC" => "GADC", "WCS" => "WCS"));
        $crud->field_type('step1', 'dropdown', array("A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
        $crud->field_type('step2', 'dropdown', array("A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
        $crud->field_type('step3', 'dropdown', array("A/P" => "A/P", "E/A" => "E/A", "CBN" => "CBN", "STR" => "STR", "TBRS" => "TBRS", "NDT" => "NDT", "SEAT" => "SEAT", "CBN SHOP" => "CBN SHOP", "TBP" => "TBP", "TVP" => "TVP", "CLN" => "CLN", "WHL" => "WHL", "RAHU" => "RAHU"));
        $crud->field_type('remark', 'dropdown', array("SENT TO CABIN" => "SENT TO CABIN", "SENT TO CABIN SHOP" => "SENT TO CABIN SHOP", "SENT TO PAINTING" => "SENT TO PAINTING", "SENT TO NDT" => "SENT TO NDT", "SENT TO TBR SHOP" => "SENT TO TBR SHOP", "SENT TO SEALANT" => "SENT TO SEALANT", "SENT TO STR HANGAR" => "SENT TO STR HANGAR", "SENT TO SEAT" => "SENT TO SEAT", "SENT TO WHEEL SHOP" => "SENT TO WHEEL SHOP", "SENT TO TV ENGINE" => "SENT TO TV ENGINE", "SENT TO CLEANING" => "SENT TO CLEANING", "MDR BELUM KE PPC" => "MDR BELUM KE PPC", "MATERIAL" => "MATERIAL", "RO BY ->" => "RO BY ->", "WAITING JOB ->" => "WAITING JOB", "COVER BY" => "COVER BY", "INTERUPT" => "INTERUPT", "MAP SHORTAGE" => "MAP SHORTAGE", "MAP GADC" => "MAP GADC", "MAP W. CUST. SUPPLY" => "MAP W. CUST. SUPPLY", "PART COMPLETE" => "PART COMPLETE", "PART PARTIAL" => "PART PARTIAL", "ANOTHER REASON" => "ANOTHER REASON"));
        $crud->set_relation('jc_reff', 'jobcard', 'jc_order');
        $crud->unset_fields('ori_taskcard', 'jc_reff', 'discrepancies', 'area_code', 'main_skill', 'iss_by', 'mdr_order', 'material_status_mrm', 'status_sap', 'cabin_status', 'id_project', 'date_created', 'id');
        $crud->callback_after_insert(array($this, 'crud_mdr_after_insert'));
        $crud->callback_add_field('seq', array($this, 'crud_mdr_seq_add_field_callback'));
        $crud->unset_back_to_list();
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');

        $this->load->view('example', array_merge($data, (array) $output));
    }

    function crud_mdr_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $mdr_update = array(
            "id_project" => $this->session->userdata('id_project'),
            'date_created' => date('Y-m-d H:i:s')
        );
        $this->db->update('mdr', $mdr_update, array('id' => $primary_key));
        return true;
    }

    function crud_mdr_seq_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_seq = 0;
        try {
            $query = $this->Mdr_model
                            ->where('id_project', $this->session->userdata('id_project'))
                            ->orderBy('seq', 'DESC')
                            ->first()->seq;
            // $last = $query->seq;
            $last_seq = $query + 1;
            error_log($this->session->userdata('id_project'));
        } catch (Exception $e) {
            error_log($e);
            $last_seq = 1;
        }
        return '<input type="text" value="' . $last_seq . '" name="seq">';
    }

    public function crud_mdr_update() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $colname = strip_tags($_POST['colname']);
        $id = strip_tags($_POST['id']);
        $coltype = strip_tags($_POST['coltype']);
        $value = strip_tags($_POST['newvalue']);
        // $tablename = strip_tags($_POST['tablename']);

        if ($coltype == 'date') {
            if ($value === "") {
                $value = NULL;
            } else {
                $date_info = date_parse_from_format('d/m/Y', $value);
                $value = "{$date_info['year']}-{$date_info['month']}-{$date_info['day']}";
            }
        }

        try {
            $this->Mdr_model->where('id', $id)->update([$colname => $value]);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_mdr_delete() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $id = strip_tags($_POST['id']);
        // $tablename = strip_tags($_POST['tablename']);

        try {
            $this->Mdr_model->destroy($id);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_mrm() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_mrm';

        $this->load->view('template', $data);
    }

    public function crud_mrm_load() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $grid->addColumn('no_', 'No', 'integer');
        $grid->addColumn('Part_Number', 'Part Number', 'string', NULL, false);
        $grid->addColumn('Alternate_Part_Number', 'Alternate Part Number', 'string', NULL, false);
        $grid->addColumn('Material_DESCRIPTION', 'Material Description', 'html');
        $grid->addColumn('Mat_Type', 'Mat Type', 'string', array("EXP" => "EXP", "ROT" => "ROT", "REP" => "REP", "RAW" => "RAW", "CON" => "CON"), NULL, false);
        $grid->addColumn('IPC', 'IPC', 'string');
        $grid->addColumn('Order_Number', 'Order Number', 'string', NULL, false);
        $grid->addColumn('STO_Number', 'STO Number', 'string');
        $grid->addColumn('Outbound_Delivery', 'Outbound Delivery', 'string');
        $grid->addColumn('TO_Number', 'TO Number', 'string');
        $grid->addColumn('jobcard_number', 'Jobcard Number', 'string', NULL, false);
        $grid->addColumn('Card_Type', 'Card Type', 'string', array("JC" => "JC", "MDR" => "MDR", "AD" => "AD"), NULL, false);
        $grid->addColumn('MRM_Issue_Date', 'MRM Issue Date', 'datetime', NULL, false);
        $grid->addColumn('Qty_Required_persingle_item_PN', 'Qty Required PerSingle', 'string');
        $grid->addColumn('TotalQty_Required_for_all_job_task', 'Total Qty Required for All Job Task', 'string', NULL, false);
        $grid->addColumn('UOM', 'UOM', 'string', array("FT" => "FT", "E/A" => "E/A", "L" => "L", "LB" => "LB", "QT" => "QT", "ROL" => "ROL", "M" => "M"));
        $grid->addColumn('Input_Stock_Manually', 'Input Stock Manually', 'string');
        $grid->addColumn('Storage_Location', 'Storage Location', 'string');
        $grid->addColumn('Material_Fulfillment_status', 'Material Fulfillment Status', 'string', array("DLVR FULL to Production" => "DLVR FULL to Production", " DLVR PARTIAL to Production" => "DLVR PARTIAL to Production", "WAITING Customer Supply" => "WAITING Customer Supply", "ROBBING Desicion" => "ROBBING Desicion", "PROVISION in Store" => "PROVISION in Store", "PRELOADED in Hangar Store" => "PRELOADED in Hangar Store", "ORDERED by Purchasing" => "ORDERED by Purchasing", "ORDERED by Loan Control" => "ORDERED by Loan Control", "ORDERED by Workshop" => "ORDERED by Workshop", "EXCHANGE in Progress" => "EXCHANGE in Progress", "Actual STOCK NIL" => "Actual STOCK NIL", "RIC/Part on Receiving Area" => "RIC/Part on Receiving Area", "NO SOURCE" => "NO SOURCE", "Waiting PN Confrimation" => "Waiting PN Confrimation", "GR OK" => "GR OK", "Need FOLLOW UP" => "Need FOLLOW UP", "PENDING/NO ACTION" => "PENDING/NO ACTION", "PR Ready" => "PR Ready", "POOLING" => "POOLING", "Shipment" => "Shipment", "Custom Process" => "Custom Process", "Need SOA" => "Need SOA", "WAITING Customer Approval" => "WAITING Customer Approval"));
        $grid->addColumn('Fullfillment_status_date', 'Fullfillment Status Date', 'date');
        $grid->addColumn('Fullfillment_remark', 'Fullfillment Remark', 'html');
        $grid->addColumn('Date_Of_PO', 'Date Of PO', 'date');
        $grid->addColumn('Purchase_Order_PO', 'Purchase Order PO', 'string');
        $grid->addColumn('AWB_Number', 'AWB Number', 'string');
        $grid->addColumn('AWB_Date', 'AWB Date', 'date');
        $grid->addColumn('Qty_Delivered', 'Qty Delivered', 'string');
        $grid->addColumn('Qty_Remain', 'Qty Remain', 'string');
        $grid->addColumn('Material_Remark', 'Material Remark', 'html');
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'id');

        $active_job = $this->Mrm_model->where('id_project', $data['id_project']);

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];


        $rowByPage = $totalUnfiltered; // 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where('Part_Number', 'like', '%' . $filter . '%')
                    ->orwhere('Alternate_Part_Number', 'like', '%' . $filter . '%')
                    ->orwhere('Material_DESCRIPTION', 'like', '%' . $filter . '%')
                    ->orwhere('Alternate_Part_Number', 'like', '%' . $filter . '%')
                    ->orwhere('Mat_Type', 'like', '%' . $filter . '%')
                    ->orwhere('IPC', 'like', '%' . $filter . '%')
                    ->orwhere('Order_Number', 'like', '%' . $filter . '%')
                    ->orwhere('STO_Number', 'like', '%' . $filter . '%')
                    ->orwhere('Outbound_Delivery', 'like', '%' . $filter . '%')
                    ->orwhere('TO_Number', 'like', '%' . $filter . '%')
                    ->orwhere('jobcard_number', 'like', '%' . $filter . '%')
                    ->orwhere('Card_Type', 'like', '%' . $filter . '%')
                    ->orwhere('MRM_Issue_Date', 'like', '%' . $filter . '%')
                    ->orwhere('Qty_Required_persingle_item_PN', 'like', '%' . $filter . '%')
                    ->orwhere('TotalQty_Required_for_all_job_task', 'like', '%' . $filter . '%')
                    ->orwhere('UOM', 'like', '%' . $filter . '%')
                    ->orwhere('Input_Stock_Manually', 'like', '%' . $filter . '%')
                    ->orwhere('Storage_Location', 'like', '%' . $filter . '%')
                    ->orwhere('Material_Fulfillment_status', 'like', '%' . $filter . '%')
                    ->orwhere('Fullfillment_status_date', 'like', '%' . $filter . '%')
                    ->orwhere('Fullfillment_remark', 'like', '%' . $filter . '%')
                    ->orwhere('Date_Of_PO', 'like', '%' . $filter . '%')
                    ->orwhere('Purchase_Order_PO', 'like', '%' . $filter . '%')
                    ->orwhere('AWB_Number', 'like', '%' . $filter . '%')
                    ->orwhere('AWB_Date', 'like', '%' . $filter . '%')
                    ->orwhere('Qty_Delivered', 'like', '%' . $filter . '%')
                    ->orwhere('Qty_Remain', 'like', '%' . $filter . '%')
                    ->orwhere('Material_Remark', 'like', '%' . $filter . '%');
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "" && in_array($_GET['sort'], $grid->getColumnFields())) {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('no_', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);

        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function crud_mrm_add() {
        $crud = new grocery_CRUD();

        $crud->set_table('mrm');
        $crud->set_subject('MRM');
        $crud->columns('Part_Number', 'Alternate_Part_Number', 'Material_DESCRIPTION', 'Mat_Type', 'IPC', 'Order_Number', 'STO_Number', 'Outbound_Delivery', 'TO_Number', 'jobcard_number', 'Card_Type', 'Qty_Required_persingle_item_PN', 'TotalQty_Required_for_all_job_task', 'UOM', 'Input_Stock_Manually', 'Storage_Location', 'Material_Fulfillment_status', 'Fullfillment_status_date', 'Fullfillment_remark', 'Date_Of_PO', 'Purchase_Order_PO', 'AWB_Number', 'AWB_Date', 'Qty_Delivered', 'Qty_Remain', 'Material_Remark');
        $crud->field_type('Mat_Type', 'dropdown', array("EXP" => "EXP", "ROT" => "ROT", "REP" => "REP", "RAW" => "RAW", "CON" => "CON"));
        $crud->field_type('Card_Type', 'dropdown', array("JC" => "JC", "MDR" => "MDR", "AD" => "AD"));
        $crud->field_type('UOM', 'dropdown', array("FT" => "FT", "E/A" => "E/A", "L" => "L", "LB" => "LB", "QT" => "QT", "ROL" => "ROL", "M" => "M"));
        $crud->field_type('Material_Fulfillment_status', 'dropdown', array("DLVR FULL to Production" => "DLVR FULL to Production", " DLVR PARTIAL to Production" => "DLVR PARTIAL to Production", "WAITING Customer Supply" => "WAITING Customer Supply", "ROBBING Desicion" => "ROBBING Desicion", "PROVISION in Store" => "PROVISION in Store", "PRELOADED in Hangar Store" => "PRELOADED in Hangar Store", "ORDERED by Purchasing" => "ORDERED by Purchasing", "ORDERED by Loan Control" => "ORDERED by Loan Control", "ORDERED by Workshop" => "ORDERED by Workshop", "EXCHANGE in Progress" => "EXCHANGE in Progress", "Actual STOCK NIL" => "Actual STOCK NIL", "RIC/Part on Receiving Area" => "RIC/Part on Receiving Area", "NO SOURCE" => "NO SOURCE", "Waiting PN Confrimation" => "Waiting PN Confrimation", "GR OK" => "GR OK", "Need FOLLOW UP" => "Need FOLLOW UP", "PENDING/NO ACTION" => "PENDING/NO ACTION", "PR Ready" => "PR Ready", "POOLING" => "POOLING", "Shipment" => "Shipment", "Custom Process" => "Custom Process", "Need SOA" => "Need SOA", "WAITING Customer Approval" => "WAITING Customer Approval"));

        $crud->unset_fields('id_project', 'date_created', 'MRM_Issue_Date', 'id', 'TotalQty_Required_for_all_job_task', 'no_');
        $crud->callback_after_insert(array($this, 'crud_mrm_after_insert'));
        $crud->callback_add_field('no_', array($this, 'crud_mrm_no_add_field_callback'));
        $crud->unset_back_to_list();
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');

        $this->load->view('example', array_merge($data, (array) $output));
    }

    function crud_mrm_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $mrm_update = array(
            "id_project" => $this->session->userdata('id_project'),
            'date_created' => date('Y-m-d H:i:s'),
            'MRM_Issue_Date' => date('Y-m-d H:i:s')
        );
        $this->db->update('mrm', $mrm_update, array('id' => $primary_key));
        return true;
    }

    function crud_mrm_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no = 0;
        try {
            $query = $this->Mrm_model
                            ->where('id_project', $this->session->userdata('id_project'))
                            ->orderBy('no_', 'DESC')
                            ->first()->no_;
            // $last = $query->seq;
            $last_no = $query + 1;
            error_log($this->session->userdata('id_project'));
        } catch (Exception $e) {
            error_log($e);
            $last_no = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="no_">';
    }

    public function crud_mrm_update() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $colname = strip_tags($_POST['colname']);
        $id = strip_tags($_POST['id']);
        $coltype = strip_tags($_POST['coltype']);
        $value = strip_tags($_POST['newvalue']);
        // $tablename = strip_tags($_POST['tablename']);

        if ($coltype == 'date') {
            if ($value === "") {
                $value = NULL;
            } else {
                $date_info = date_parse_from_format('d/m/Y', $value);
                $value = "{$date_info['year']}-{$date_info['month']}-{$date_info['day']}";
            }
        }

        try {
            $this->Mrm_model->where('id', $id)->update([$colname => $value]);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_mrm_delete() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $id = strip_tags($_POST['id']);
        // $tablename = strip_tags($_POST['tablename']);

        try {
            $this->Mrm_model->destroy($id);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_prm() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_prm';

        $this->load->view('template', $data);
    }

    public function crud_prm_load() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $grid->addColumn('No_', 'No', 'integer');
        $grid->addColumn('Part_Number', 'Part Number', 'string');
        $grid->addColumn('Description', 'Description', 'html');
        $grid->addColumn('CTG', 'CTG', 'string');
        $grid->addColumn('SN_Qty', 'SN/Qty', 'string');
        $grid->addColumn('Post', 'Post', 'string');
        $grid->addColumn('Ref', 'Ref.', 'string');
        $grid->addColumn('Part_Status', 'Part Status', 'string');
        $grid->addColumn('PL_SP_OUT_No', 'P/L or SP (Out) No', 'string');
        $grid->addColumn('PL_SP_OUT_Date', 'P/L or SP (Out) Date', 'date');
        $grid->addColumn('PL_SP_IN_No', 'P/L or SP (In) No', 'string');
        $grid->addColumn('PL_SP_IN_Date', 'P/L or SP (In) Date', 'date');
        $grid->addColumn('Unit_Send', 'Unit Send', 'string');
        $grid->addColumn('Unit_Recv', 'Unit Recv.', 'string');
        $grid->addColumn('Part_Location', 'Part Location.', 'string');
        $grid->addColumn('Remark', 'Remark', 'html');
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'id');

        $active_job = $this->Prm_model->where('id_project', $data['id_project']);

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];


        $rowByPage = $totalUnfiltered; // 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where('Part_Number', 'like', '%' . $filter . '%')
                    ->orwhere('Description', 'like', '%' . $filter . '%')
                    ->orwhere('CTG', 'like', '%' . $filter . '%')
                    ->orwhere('SN_Qty', 'like', '%' . $filter . '%')
                    ->orwhere('Post', 'like', '%' . $filter . '%')
                    ->orwhere('Ref', 'like', '%' . $filter . '%')
                    ->orwhere('Part_Status', 'like', '%' . $filter . '%')
                    ->orwhere('PL_SP_OUT_No', 'like', '%' . $filter . '%')
                    ->orwhere('PL_SP_OUT_Date', 'like', '%' . $filter . '%')
                    ->orwhere('PL_SP_IN_No', 'like', '%' . $filter . '%')
                    ->orwhere('PL_SP_IN_Date', 'like', '%' . $filter . '%')
                    ->orwhere('Unit_Send', 'like', '%' . $filter . '%')
                    ->orwhere('Unit_Recv	', 'like', '%' . $filter . '%')
                    ->orwhere('Part_Location', 'like', '%' . $filter . '%')
                    ->orwhere('Remark', 'like', '%' . $filter . '%');
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "" && in_array($_GET['sort'], $grid->getColumnFields())) {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('No_', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);

        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function crud_prm_add() {
        $crud = new grocery_CRUD();

        $crud->set_table('prm');
        $crud->set_subject('PRM');
        $crud->columns('No_', 'Part_Number', 'Description', 'CTG', 'SN_Qty', 'Post', 'Ref', 'Part_Status', 'PL_SP_OUT_No', 'PL_SP_OUT_Date', 'PL_SP_IN_No', 'PL_SP_IN_Date', 'Unit_Send', 'Unit_Recv', 'Part_Location', 'Remark');
        $crud->unset_fields('id_project', 'date_created', 'id');
        $crud->callback_after_insert(array($this, 'crud_prm_after_insert'));
        $crud->callback_add_field('No_', array($this, 'crud_prm_no_add_field_callback'));
        $crud->unset_back_to_list();
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');

        $this->load->view('example', array_merge($data, (array) $output));
    }

    function crud_prm_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $prm_update = array(
            "id_project" => $this->session->userdata('id_project'),
            'date_created' => date('Y-m-d H:i:s')
        );
        $this->db->update('prm', $prm_update, array('id' => $primary_key));
        return true;
    }

    function crud_prm_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no = 0;
        try {
            $query = $this->Prm_model
                            ->where('id_project', $this->session->userdata('id_project'))
                            ->orderBy('No_', 'DESC')
                            ->first()->No_;
            // $last = $query->seq;
            $last_no = $query + 1;
            error_log($this->session->userdata('id_project'));
        } catch (Exception $e) {
            error_log($e);
            $last_no = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="No_">';
    }

    public function crud_prm_update() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $colname = strip_tags($_POST['colname']);
        $id = strip_tags($_POST['id']);
        $coltype = strip_tags($_POST['coltype']);
        $value = strip_tags($_POST['newvalue']);
        // $tablename = strip_tags($_POST['tablename']);

        if ($coltype == 'date') {
            if ($value === "") {
                $value = NULL;
            } else {
                $date_info = date_parse_from_format('d/m/Y', $value);
                $value = "{$date_info['year']}-{$date_info['month']}-{$date_info['day']}";
            }
        }

        try {
            $this->Prm_model->where('id', $id)->update([$colname => $value]);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_prm_delete() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $id = strip_tags($_POST['id']);
        // $tablename = strip_tags($_POST['tablename']);

        try {
            $this->Prm_model->destroy($id);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_crm() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_crm';

        $this->load->view('template', $data);
    }

    public function crud_crm_load() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $grid = new EditableGrid();

        $grid->addColumn('No_', 'No', 'integer');
        $grid->addColumn('Part_Number', 'Part Number', 'string');
        $grid->addColumn('Description', 'Description', 'string');
        $grid->addColumn('CTG', 'CTG', 'string');
        $grid->addColumn('SN_Qty', 'SN/Qty', 'string');
        $grid->addColumn('Post', 'Post', 'string');
        $grid->addColumn('Ref', 'Ref.', 'string');
        $grid->addColumn('Part_Status', 'Part Status', 'string');
        $grid->addColumn('SP_OUT_No', 'SP (Out) No', 'string');
        $grid->addColumn('SP_OUT_Date', 'SP (Out) Date', 'date');
        $grid->addColumn('SP_IN_No', 'SP (In) No', 'string');
        $grid->addColumn('SP_IN_Date', 'SP (In) Date', 'date');
        $grid->addColumn('Unit_Send', 'Unit Send', 'string');
        $grid->addColumn('Unit_Recv', 'Unit Recv.', 'string');
        $grid->addColumn('Part_Location', 'Part Location.', 'string');
        $grid->addColumn('Remark', 'Remark', 'string');
        $grid->addColumn('Status', 'Status', 'string', array("UR - Und. repr" => "UR - Und. repr", "SB - Serviceabl" => "SB - Serviceabl", "RA - Return as is" => "RA - Return as is", "CR - Sent to Contracr" => "CR - Sent to Contracr", "C - Close" => "C - Close"));
        $grid->addColumn('action', 'Action', 'html', NULL, false, 'id');

        $active_job = $this->Crm_model->where('id_project', $data['id_project']);

        $totalUnfiltered = $active_job->count();
        $total = $totalUnfiltered;

        $page = 0;
        if (isset($_GET['page']) && is_numeric($_GET['page']))
            $page = (int) $_GET['page'];


        $rowByPage = $totalUnfiltered; // 50;

        $from = ($page - 1) * $rowByPage;

        if (isset($_GET['filter']) && $_GET['filter'] != "") {
            $filter = $_GET['filter'];
            $active_job = $active_job
                    ->where('Part_Number', 'like', '%' . $filter . '%')
                    ->orwhere('Description', 'like', '%' . $filter . '%')
                    ->orwhere('CTG', 'like', '%' . $filter . '%')
                    ->orwhere('SN_Qty', 'like', '%' . $filter . '%')
                    ->orwhere('Post', 'like', '%' . $filter . '%')
                    ->orwhere('Ref', 'like', '%' . $filter . '%')
                    ->orwhere('SP_OUT_No', 'like', '%' . $filter . '%')
                    ->orwhere('SP_OUT_Date', 'like', '%' . $filter . '%')
                    ->orwhere('SP_IN_No', 'like', '%' . $filter . '%')
                    ->orwhere('SP_IN_Date', 'like', '%' . $filter . '%')
                    ->orwhere('Unit_Send', 'like', '%' . $filter . '%')
                    ->orwhere('Unit_Recv', 'like', '%' . $filter . '%')
                    ->orwhere('Part_Location', 'like', '%' . $filter . '%')
                    ->orwhere('Remark', 'like', '%' . $filter . '%')
                    ->orwhere('Status', 'like', '%' . $filter . '%');
            $total = $active_job->count();
        }

        if (isset($_GET['sort']) && $_GET['sort'] != "" && in_array($_GET['sort'], $grid->getColumnFields())) {
            $active_job = $active_job->orderBy($_GET['sort'], $_GET['asc'] == "0" ? "DESC" : "ASC");
        } else {
            $active_job = $active_job->orderBy('No_', 'ASC');
        }

        $active_job = $active_job->skip($from)->take($rowByPage);
        $grid->setPaginator(ceil($total / $rowByPage), (int) $total, (int) $totalUnfiltered, null);

        $grid->renderJSON($active_job->get(), false, false, !isset($_GET['data_only']));
    }

    public function crud_crm_add() {
        $crud = new grocery_CRUD();

        $crud->set_table('crm');
        $crud->set_subject('CRM');
        $crud->columns('No_', 'Part_Number', 'Description', 'CTG', 'SN_Qty', 'Post', 'Ref', 'Part_Status', 'SP_OUT_No', 'SP_OUT_Date', 'SP_IN_No', 'SP_IN_Date', 'Unit_Send', 'Unit_Recv', 'Part_Location', 'Remark', 'Status');
        $crud->field_type('Status', 'dropdown', array("UR - Und. repr" => "UR - Und. repr", "SB - Serviceabl" => "SB - Serviceabl", "RA - Return as is" => "RA - Return as is", "CR - Sent to Contracr" => "CR - Sent to Contracr", "C - Close" => "C - Close"));
        $crud->unset_fields('id_project', 'date_created', 'id');
        $crud->callback_after_insert(array($this, 'crud_crm_after_insert'));
        $crud->callback_add_field('No_', array($this, 'crud_crm_no_add_field_callback'));
        $crud->unset_back_to_list();
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');

        $this->load->view('example', array_merge($data, (array) $output));
    }

    function crud_crm_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $prm_update = array(
            "id_project" => $this->session->userdata('id_project'),
            'date_created' => date('Y-m-d H:i:s')
        );
        $this->db->update('crm', $prm_update, array('id' => $primary_key));
        return true;
    }

    function crud_crm_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no = 0;
        try {
            $query = $this->Crm_model
                            ->where('id_project', $this->session->userdata('id_project'))
                            ->orderBy('No_', 'DESC')
                            ->first()->No_;
            // $last = $query->seq;
            $last_no = $query + 1;
            error_log($this->session->userdata('id_project'));
        } catch (Exception $e) {
            error_log($e);
            $last_no = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="No_">';
    }

    public function crud_crm_update() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $colname = strip_tags($_POST['colname']);
        $id = strip_tags($_POST['id']);
        $coltype = strip_tags($_POST['coltype']);
        $value = strip_tags($_POST['newvalue']);
        // $tablename = strip_tags($_POST['tablename']);

        if ($coltype == 'date') {
            if ($value === "") {
                $value = NULL;
            } else {
                $date_info = date_parse_from_format('d/m/Y', $value);
                $value = "{$date_info['year']}-{$date_info['month']}-{$date_info['day']}";
            }
        }

        try {
            $this->Crm_model->where('id', $id)->update([$colname => $value]);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_crm_delete() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }

        $data["session"] = $this->session->userdata('logged_in');

        $id = strip_tags($_POST['id']);
        // $tablename = strip_tags($_POST['tablename']);

        try {
            $this->Crm_model->destroy($id);
        } catch (Exception $exc) {
            echo "error";
        } finally {
            echo "ok";
        }
    }

    public function crud_task() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();


        $crud->set_table('task');
        $crud->where('id_project', $this->session->userdata('id_project'));
        $crud->set_subject('Task');
        $crud->columns('No_', 'Subject', 'Start_Date', 'End_Date', 'Billable_Status');
        $crud->display_as('Subject', 'Name');
        $crud->unset_fields('id_project', 'date_created', 'id');
        $crud->set_field_upload('Attachment', 'assets/uploads/files');
        $crud->callback_after_insert(array($this, 'task_after_insert'));
        $crud->callback_add_field('No_', array($this, 'crud_task_no_add_field_callback'));
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_task';
        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function task_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $task_day_update = array(
            "date_created" => date('Y-m-d H:i:s'),
            "id_project" => $this->session->userdata('id_project'),
        );

        $this->db->update('task', $task_day_update, array('id' => $primary_key));

        return true;
    }

    function crud_task_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no_ = 0;
        try {
            $query = $this->db->select('No_')
                    ->from('task')
                    ->where('id_project', $this->session->userdata('id_project'))
                    ->order_by('No_', 'DESC')
                    ->limit(1)
                    ->get();
            $ret = $query->row();
            $last = $ret->No_;
            $last_no = $last + 1;
        } catch (Exception $e) {
            $last_no_ = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="No_">';
    }

    function crud_ticket() {

        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();

        $crud->set_table('ticket');
        $crud->set_subject('Ticket');
        $crud->where('id_project', $this->session->userdata('id_project'));
        $crud->columns('Subject', 'Ticket_Type', 'Status', 'Attachment');
        $crud->field_type('Ticket_Type', 'dropdown', array('Low' => 'Low', 'Medium' => 'Medium', 'High' => 'High'));
        $crud->field_type('Status', 'dropdown', array('New' => 'New', 'Open Replied' => 'Open Replied', 'Open' => 'Open', 'Close' => 'Close'));
        $crud->add_fields('Subject', 'Ticket_Type', 'Description', 'Attachment');
        $crud->edit_fields('Subject', 'Ticket_Type', 'Status', 'Description', 'Attachment');
        $crud->set_field_upload('Attachment', 'assets/uploads/files');
        $crud->callback_after_insert(array($this, 'ticket_after_insert'));
        $crud->callback_after_update(array($this, 'ticket_after_update'));
        $crud->callback_column('Subject', array($this, 'callback_detail_ticket'));
        // $crud->add_action('Comment', base_url() . 'assets/dist/img/icon_comments.png', 'admin/projects/comment_ticket');
        $output = $crud->render();
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_ticket';
        $this->load->view('template', array_merge($data, (array) $output));
    }

    function ticket_after_insert($post_array, $primary_key) {
        $ticket_update = array(
            "Create_Date_Ticket" => date('Y-m-d H:i:s'),
            "Created_By" => $this->session->userdata('logged_in')['id_user'],
            "Status" => "Open",
            "id_project" => $this->session->userdata('id_project'),
        );

        $this->db->update('ticket', $ticket_update, array('Id' => $primary_key));

        return true;
    }

    public function callback_detail_ticket($value, $row) {

        return '<a href="' . base_url() . 'admin/projects/comment_ticket/' . $row->Id . '">' . $value . '</a>';
    }

    function comment_ticket() {

        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('ticket_id', $this->uri->segment(4));
            }
            $data['ticket_id'] = $this->session->userdata('ticket_id');
            $active_prj = $this->Ticket_model->find($data['ticket_id']);
            if ($active_prj) {
                $data['Subject'] = $active_prj->Subject;
                $data['Status'] = $active_prj->Status;
                $data['Created_By'] = $active_prj->Created_By;
                $data['Ticket_Type'] = $active_prj->Ticket_Type;
                $data['Create_Date_Ticket'] = $active_prj->Create_Date_Ticket;
                $data['Status'] = $active_prj->Status;
            }
        } catch (Exception $e) {
            error_log($e);
        }


        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/comment_ticket';
        $this->load->view('template', $data);
    }

    function ticket_after_update($post_array, $primary_key) {
        $ticket_update = array(
            "Replay_At" => date('Y-m-d H:i:s'),
            "Replay_By" => $this->session->userdata('logged_in')['id_user']
        );

        $this->db->update('ticket', $ticket_update, array('id' => $primary_key));

        return true;
    }

    public function crud_estimates() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();


        $crud->set_table('estimates');
        $crud->where('id_project', $this->session->userdata('id_project'));
        $crud->set_subject('Estimate');
        $crud->columns('Estimate', 'Amount', 'Start_Date', 'Expired_Date', 'Status');
        $crud->unset_fields('id_project', 'date_created', 'id');
        // $crud->callback_column('Amount', array($this, 'valueToDollar'));
        $crud->field_type('Status', 'dropdown', array('Draft' => 'Draft', 'Sent' => 'Sent', 'Expired' => 'Expired', 'Declined' => 'Declined', 'Accepted' => 'Accepted'));
        $crud->callback_after_insert(array($this, 'estimate_after_insert'));
        $crud->callback_add_field('Estimate', array($this, 'crud_estimate_no_add_field_callback'));
        // $crud->set_rules('Expired_Date', 'Expired_Date', 'trim|callback_check_estimatedates[' . $this->input->post('Start_Date') . ']');
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_estimates';
        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function valueToDollar($value, $row) {
        return '$' . $value;
    }

    public function estimate_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $task_day_update = array(
            "date_created" => date('Y-m-d H:i:s'),
            "id_project" => $this->session->userdata('id_project'),
        );

        $this->db->update('estimates', $task_day_update, array('id' => $primary_key));

        return true;
    }

    function crud_estimate_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no_ = 0;
        try {
            $query = $this->db->select('Estimate')
                    ->from('estimates')
                    ->where('id_project', $this->session->userdata('id_project'))
                    ->order_by('Estimate', 'DESC')
                    ->limit(1)
                    ->get();
            $ret = $query->row();
            $last = $ret->Estimate;
            $last_no = $last + 1;
        } catch (Exception $e) {
            $last_no_ = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="Estimate">';
    }

    public function check_estimatedates($Expired_Date, $Start_Date) {
        $parts = explode('/', $this->input->post('Start_Date'));
        $Start_Date = join('-', $parts);
        $Start_Date = strtotime($Start_Date);

        $parts2 = explode('/', $this->input->post('Expired_Date'));
        $Expired_Date = join('-', $parts2);
        $Expired_Date = strtotime($Expired_Date);

        if ($Expired_Date >= $Start_Date) {

            return true;
        } else {
            $this->form_validation->set_message('check_estimatedates', "Expired date should be greater than start date");
            return false;
        }
    }

    public function crud_invoices() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();


        $crud->set_table('invoices');
        $crud->where('id_project', $this->session->userdata('id_project'));
        $crud->set_subject('Invoice');
        $crud->columns('Invoice', 'Amount', 'Bill_Date', 'Due_Date', 'Status');
        $crud->unset_fields('id_project', 'date_created', 'id');
        $crud->callback_column('Amount', array($this, 'valueToDollar'));
        $crud->field_type('Status', 'dropdown', array('Paid' => 'Paid', 'Unpaid' => 'Unpaid'));
        $crud->callback_after_insert(array($this, 'invoices_after_insert'));
        $crud->callback_add_field('Invoice', array($this, 'crud_invoices_no_add_field_callback'));
        $crud->set_rules('Due_Date', 'Due_Date', 'trim|callback_check_dates[' . $this->input->post('Bill_Date') . ']');
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_invoices';
        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function invoices_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $task_day_update = array(
            "date_created" => date('Y-m-d H:i:s'),
            "id_project" => $this->session->userdata('id_project'),
        );

        $this->db->update('invoices', $task_day_update, array('id' => $primary_key));

        return true;
    }

    function crud_invoices_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no_ = 0;
        try {
            $query = $this->db->select('Invoice')
                    ->from('invoices')
                    ->where('id_project', $this->session->userdata('id_project'))
                    ->order_by('Invoice', 'DESC')
                    ->limit(1)
                    ->get();
            $ret = $query->row();
            $last = $ret->Invoice;
            $last_no = $last + 1;
        } catch (Exception $e) {
            $last_no_ = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="Invoice">';
    }

    public function check_dates($Due_Date, $Bill_Date) {
        $parts = explode('/', $this->input->post('Bill_Date'));
        $Bill_Date = join('-', $parts);
        $Bill_Date = strtotime($Bill_Date);

        $parts2 = explode('/', $this->input->post('Due_Date'));
        $Due_Date = join('-', $parts2);
        $Due_Date = strtotime($Due_Date);

        if ($Due_Date >= $Bill_Date) {

            return true;
        } else {
            $this->form_validation->set_message('check_dates', "due date should be greater than bill date");
            return false;
        }
    }

    public function crud_files() {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();


        $crud->set_table('files');
        $crud->where('id_project', $this->session->userdata('id_project'));
        $crud->set_subject('File');
        $crud->columns('No_', 'Subject', 'Description', 'Attachments_file');
        $crud->unset_fields('id_project', 'date_created', 'id');
        $crud->set_field_upload('Attachments_file', 'assets/uploads/files');
        $crud->callback_after_insert(array($this, 'file_after_insert'));

        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_files';
        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function file_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $task_day_update = array(
            "date_created" => date('Y-m-d H:i:s'),
            "id_project" => $this->session->userdata('id_project'),
        );
        $this->db->update('files', $task_day_update, array('id' => $primary_key));
        return true;
    }

    public function dashboard($row) {
        try {
            if ($this->uri->segment(4)) {
                $this->session->set_userdata('id_project', $this->uri->segment(4));
            }
            $data['id_project'] = $this->session->userdata('id_project');
            $active_prj = $this->Project_model->find($data['id_project']);
            if ($active_prj) {
                $data['project_name'] = $active_prj->project_name;
            }
        } catch (Exception $e) {
            error_log($e);
        }
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/dashboard';
        $data['projects'] = true;

        $data['total_jc_open'] = $this->Dashboard_model->getTotalJCOpen();
        $data['total_mdr_open'] = $this->Dashboard_model->getTotalMDROpen();
        $data['total_mdr_open_excel'] = $this->Dashboard_model->getTotalMDROpen_excel();

        $data['total_jc_hangar'] = $this->Dashboard_model->getTotalJChangar();
        $data['total_mdr_hangar'] = $this->Dashboard_model->getTotalMDRhangar();

        $data['total_jc_wsss'] = $this->Dashboard_model->getTotalJCwsss();
        $data['total_mdr_wsss'] = $this->Dashboard_model->getTotalMDRwsss();

        $data['total_jc_wsse'] = $this->Dashboard_model->getTotalJCwsse();
        $data['total_mdr_wsse'] = $this->Dashboard_model->getTotalMDRwsse();

        $data['total_jc_wscb'] = $this->Dashboard_model->getTotalJCwscb();
        $data['total_mdr_wscb'] = $this->Dashboard_model->getTotalMDRwscb();

        $data['total_jc_wsls'] = $this->Dashboard_model->getTotalJCwsls();
        $data['total_mdr_wsls'] = $this->Dashboard_model->getTotalMDRwsls();

        $data['total_jc_close'] = $this->Dashboard_model->getTotalJCclose();
        $data['total_mdr_close'] = $this->Dashboard_model->getTotalMDRclose();
        $data['total_mdr_close_excel'] = $this->Dashboard_model->getTotalMDRclose_excel();
        $total_close = $this->Dashboard_model->getTotalClose();
        $total_open = $this->Dashboard_model->getTotalOpen();
        $total_progress = $this->Dashboard_model->getTotalProgress();
        $data['total_jc_progress'] = $this->Dashboard_model->getTotalJCProgress();
        $data['total_mdr_progress'] = $this->Dashboard_model->getTotalMDRProgress();
        $data['total_mdr_progress_excel'] = $this->Dashboard_model->getTotalMDRProgress_excel();
        /* $data['percentage_jobcard_phase_open'] = $this->Dashboard_model->getJobcardPhase(); */
        $total_jc = $this->Dashboard_model->getTotaljc();
        $total_mdr = $this->Dashboard_model->getTotalMDR();
        $total_mdr_excel = $this->Dashboard_model->getTotalMDR_excel();

        //dashboard for close all
        $data['total_excel_jc'] = $this->Dashboard_model->getTotalJOBCARD_excel_all($row);
        $data['total_excel_close_jc'] = $this->Dashboard_model->getTotalJOBCARD_excel_close_all($row);
        $data['total_excel_mdr'] = $this->Dashboard_model->getTotalMDR_excel_all($row);
        $data['total_excel_close_mdr'] = $this->Dashboard_model->getTotalMDR_excel_close_all($row);
        $data['percentage_jc_closed'] = @round(($total_excel_close_jc / $total_excel_jc) * 100);

        //Dashboard for jobcard perphase

        $data['jobcard_phase'] = $this->Dashboard_model->getJobcardPhase();
        $data['parameter_insp_open'] = $this->Dashboard_model->getPhaseInspOpen("INSP");
        $data['parameter_insp_close'] = $this->Dashboard_model->getPhaseInspClose("INSP");
        $data['parameter_inst_open'] = $this->Dashboard_model->getPhaseInstOpen("INST/REST");
        $data['parameter_inst_close'] = $this->Dashboard_model->getPhaseInstClose("INST/REST");
        $data['parameter_opc_open'] = $this->Dashboard_model->getPhaseOpcOpen("OPC/FUC");
        $data['parameter_opc_close'] = $this->Dashboard_model->getPhaseOpcClose("OPC/FUC");
        $data['parameter_lub_open'] = $this->Dashboard_model->getPhaseLubOpen("SERV/LUB");
        $data['parameter_lub_close'] = $this->Dashboard_model->getPhaseLubClose("SERV/LUB");

        //Dashboard for jobcard per area
        $data['parameter_aft_open'] = $this->Dashboard_model->getPhaseAFTOpen("AFT CARGO");
        $data['parameter_aft_close'] = $this->Dashboard_model->getPhaseAFTClose("AFT CARGO");
        $data['parameter_cabin_open'] = $this->Dashboard_model->getPhaseCabinOpen("CABIN");
        $data['parameter_cabin_close'] = $this->Dashboard_model->getPhaseCabinClose("CABIN");
        $data['parameter_elect_open'] = $this->Dashboard_model->getPhaseELECTOpen("ELECT");
        $data['parameter_elect_close'] = $this->Dashboard_model->getPhaseELECTClose("ELECT");
        $data['parameter_eng1_open'] = $this->Dashboard_model->getPhaseENG1Open("ENG#1");
        $data['parameter_eng1_close'] = $this->Dashboard_model->getPhaseENG1Close("ENG#1");
        $data['parameter_fuselage_open'] = $this->Dashboard_model->getPhaseFUSELAGEOpen("FUSELAGE");
        $data['parameter_fuselage_close'] = $this->Dashboard_model->getPhaseFUSELAGEClose("FUSELAGE");
        $data['parameter_lhwing_open'] = $this->Dashboard_model->getPhaseLHWINGOpen("LH-WING");
        $data['parameter_lhwing_close'] = $this->Dashboard_model->getPhaseLHWINClose("LH-WING");

        //Dashboard for mdr per area
        $data['parameter_mdr_cabin_open'] = $this->Dashboard_model->getMDRCabinOpen("CABIN");
        $data['parameter_mdr_cabin_close'] = $this->Dashboard_model->getMDRCabinClose("CABIN");
        $data['parameter_mdr_eng1_open'] = $this->Dashboard_model->getMDRENG1Open("ENG#1");
        $data['parameter_mdr_eng1_close'] = $this->Dashboard_model->getMDRENG1Close("ENG#1");
        $data['parameter_mdr_eng3_open'] = $this->Dashboard_model->getMDRENG3Open("ENG#3");
        $data['parameter_mdr_eng3_close'] = $this->Dashboard_model->getMDRENG3Close("ENG#3");
        $data['parameter_mdr_eng4_open'] = $this->Dashboard_model->getMDRENG4Open("ENG#4");
        $data['parameter_mdr_eng4_close'] = $this->Dashboard_model->getMDRENG4Close("ENG#4");
        $data['parameter_mdr_fuselage_open'] = $this->Dashboard_model->getMDRFUSELAGEOpen("FUSELAGE");
        $data['parameter_mdr_fuselage_close'] = $this->Dashboard_model->getMDRFUSELAGEClose("FUSELAGE");
        $data['parameter_mdr_general_open'] = $this->Dashboard_model->getMDRGENERALOpen("GENERAL AREA");
        $data['parameter_mdr_general_close'] = $this->Dashboard_model->getMDRGENERALClose("GENERAL AREA");

        //Dashboard for jobcard per skill
        $data['parameter_jobcard_skill_AP_open'] = $this->Dashboard_model->getJC_AP_Open("A/P");
        $data['parameter_jobcard_skill_AP_close'] = $this->Dashboard_model->getJC_AP_Close("A/P");
        $data['parameter_jobcard_skill_CBN_open'] = $this->Dashboard_model->getJC_CBN_Open("CBN");
        $data['parameter_jobcard_skill_CBN_close'] = $this->Dashboard_model->getJC_CBN_Close("CBN");
        $data['parameter_jobcard_skill_EA_open'] = $this->Dashboard_model->getJC_EA_Open("E/A");
        $data['parameter_jobcard_skill_EA_close'] = $this->Dashboard_model->getJC_EA_Close("E/A");
        $data['parameter_jobcard_skill_TBP_open'] = $this->Dashboard_model->getJC_TBP_Open("TBP");
        $data['parameter_jobcard_skill_TBP_close'] = $this->Dashboard_model->getJC_TBP_Close("TBP");

        //$total_open = $this->Dashboard_model->getTotalOpen();
        $data['percentage_jc_close'] = round(($data['total_jc_close'] / $total_close) * 100);
        $data['percentage_mdr_close'] = round(($data['total_mdr_close'] / $total_close) * 100);

        $data['percentage_mdr_open'] = round(($data['total_mdr_open'] / $total_open) * 100);
        $data['percentage_jc_open'] = round(($data['total_jc_open'] / $total_open) * 100);

        $data['percentage_mdr_progress'] = round(($data['total_mdr_progress'] / $total_progress) * 100);
        $data['percentage_jc_progress'] = round(($data['total_jc_progress'] / $total_progress) * 100);

        $data['percentage_jc_open_pie'] = round(($data['total_jc_open'] / $total_jc) * 100);
        $data['percentage_jc_progress_pie'] = round(($data['total_jc_progress'] / $total_jc) * 100);
        $data['percentage_jc_close_pie'] = round(($data['total_jc_close'] / $total_jc) * 100);

        $data['percentage_mdr_open_pie'] = round(($data['total_mdr_open'] / $total_mdr) * 100);
        $data['percentage_mdr_open_excel_pie'] = round(($data['total_mdr_open_excel'] / $total_mdr_excel) * 100);
        $data['percentage_mdr_progress_pie'] = round(($data['total_mdr_progress'] / $total_mdr) * 100);
        $data['percentage_mdr_progress_excel_pie'] = round(($data['total_mdr_progress_excel'] / $total_mdr_excel) * 100);
        $data['percentage_mdr_close_pie'] = round(($data['total_mdr_close'] / $total_mdr) * 100);
        $data['percentage_mdr_close_excel_pie'] = round(($data['total_mdr_close_excel'] / $total_mdr_excel) * 100);
        $this->load->view('template', $data);
    }

    public function crud_proposal() {

        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();
        $crud->set_table('proposal');
        $crud->set_subject('Proposal');
        $crud->columns('No_Proposal', 'Subject', 'Start_Date', 'Expired_Date', 'Amount', 'Status');
        $crud->unset_fields('date_created', 'Id');

        $crud->field_type('Status', 'dropdown', array('Draft' => 'Draft', 'Sent' => 'Sent', 'Accepted' => 'Accepted', 'Declined' => 'Declined'));
        $crud->callback_after_insert(array($this, 'proposal_after_insert'));
        $crud->callback_add_field('No_Proposal', array($this, 'crud_propsal_no_add_field_callback'));
        $crud->set_rules('Expired_Date', 'Expired_Date', 'trim|callback_proposal_check_dates[' . $this->input->post('Start_Date') . ']');
        $output = $crud->render();

        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'admin/projects/crud_proposal';
        $this->load->view('template', array_merge($data, (array) $output));
    }

    public function proposal_after_insert($post_array, $primary_key) {
        date_default_timezone_set('Asia/Jakarta');
        $proposal_day_update = array(
            "date_created" => date('Y-m-d H:i:s')
        );

        $this->db->update('proposal', $proposal_day_update, array('Id' => $primary_key));

        return true;
    }

    function crud_propsal_no_add_field_callback() {
        error_reporting(E_ALL & ~E_NOTICE);
        $last_no_ = 0;
        try {
            $query = $this->db->select('No_Proposal')
                    ->from('proposal')
                    ->order_by('No_Proposal', 'DESC')
                    ->limit(1)
                    ->get();
            $ret = $query->row();
            $last = $ret->No_Proposal;
            $last_no = $last + 1;
        } catch (Exception $e) {
            $last_no_ = 1;
        }
        return '<input type="text" value="' . $last_no . '" name="No_Proposal">';
    }

    public function proposal_check_dates($Due_Date, $Bill_Date) {
        $parts = explode('/', $this->input->post('Bill_Date'));
        $Bill_Date = join('-', $parts);
        $Bill_Date = strtotime($Bill_Date);

        $parts2 = explode('/', $this->input->post('Due_Date'));
        $Due_Date = join('-', $parts2);
        $Due_Date = strtotime($Due_Date);

        if ($Due_Date >= $Bill_Date) {

            return true;
        } else {
            $this->form_validation->set_message('check_dates', "due date should be greater than bill date");
            return false;
        }
    }

}
