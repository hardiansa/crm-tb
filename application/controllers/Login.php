<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Login_users');
    }

    function index() {
        $this->load->view('login');
    }

    function verifylogin() {
        //This method will have the credentials validation

        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

        if ($this->form_validation->run() == FALSE) {
            //Field validation failed.  User redirected to login page
            $this->load->view('login');
        } else {
            //Go to private area
            redirect('projects/', 'refresh');
        }
    }

    function check_database($password) {
        //Field validation succeeded.  Validate against database
        $username = $this->input->post('username');
        //query the database
        $result = $this->Login_users->login($username, $password);

        if ($result) {
            $sess_array = array(
                'id_user' => $result->ID_USER,
                'name' => $result->NAME,
                'username' => $result->USERNAME,
                'group_id' => $result->GROUP_ID
            );
            $this->session->set_userdata('logged_in', $sess_array);

            $this->Login_users->update_last_log($sess_array['ID_USER']);

            if ($sess_array['group_id'] == '1') {
                redirect('admin/projects/view_vcustomer');
            } elseif ($sess_array['group_id'] == '2') {
                redirect('trader/projects/');
            } elseif ($sess_array['group_id'] == '3') {
                redirect('manager/projects/');
            } elseif ($sess_array['group_id'] == '4') {
                redirect('purchaser/projects/');
            }
            return TRUE;
        } else {
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return false;
        }
    }

    function logout() {
        $this->session->unset_userdata('logged_in');
        redirect('/', 'refresh');
    }

}
