<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once('assets/editablegrid/EditableGrid.php');

class Project_summary extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Summary_model');
        $this->load->model('Jobcard_model');
        $this->load->model('Mdr_model');

        $this->data["session"] = $this->session->userdata('logged_in');
        if ($this->data["session"]["group_id"] != "1") {
            redirect('login/logout', 'refresh');
        }
    }

    public function index() {
        $data['is_grocery'] = true;
        $crud = new grocery_CRUD();
        $crud->set_table('project');
        $crud->set_subject('Project');

        $crud->columns('customer_name', 'aircraft_registered', 'project_name', 'start_date', 'finish_date', 'location', 'status_project', 'type_project', 'Jobcard_Progress', 'MDR_Progress');
        $crud->display_as('customer_name', 'Customer Name');
        $crud->set_relation('customer_name', 'customer', 'COMPANY_NAME');
        $crud->set_rules('finish_date', 'finish_date', 'trim|callback_check_projectdates[' . $this->input->post('start_date') . ']');
        $crud->unset_fields('Id');
        $crud->field_type('location', 'dropdown', array(
            'Hangar 1' => 'Hangar 1',
            'Hangar 3' => 'Hangar 3',
            'Hangar 4' => 'Hangar 4'));
        $crud->field_type('status_project', 'dropdown', array(
            'Created' => 'Created',
            'Released' => 'Released',
            'Finished' => 'Finished'));
        $crud->field_type('type_project', 'dropdown', array(
            'Retail' => 'Retail',
            'Contract' => 'Contract'));
        $crud->unset_add();
        $crud->unset_read();
        $crud->unset_delete();
        $crud->unset_edit();
        $crud->required_fields('location', 'status_project', 'type_project');
        $crud->callback_column('project_name', array($this, 'callback_project'));
        $crud->callback_column('Jobcard_Progress', array($this, 'callback_jobcard_progress'));
        $crud->callback_column('MDR_Progress', array($this, 'callback_mdr_progress'));
        $crud->add_action('Detail', base_url() . 'assets/dist/img/icons_dashboard.png', 'admin/projects/dashboard'); // , 'admin/projects/project_tabs');
        $output = $crud->render();       
        $data["session"] = $this->session->userdata('logged_in');
        $data['content'] = 'customer/project_summary/index';
        $data['total_Created'] = $this->Summary_model->getTotalCreated();
        $data['total_InProgress'] = $this->Summary_model->getTotalInProgress();
        $data['total_Close'] = $this->Summary_model->getTotalClose();
        $this->load->view('template', array_merge($data, (array) $output));
        // $this->load->view('template', $data);
    }

    public function callback_project($value, $row) {

        return '<a href="' . base_url() . 'admin/projects/crud_jobcard/' . $row->Id . '">' . $value . '</a>';
    }

    public function callback_jobcard_progress($value, $row) {
        $close = $this->Jobcard_model->where('id_project', '=', $row->Id)
                ->where('status', 'like', 'Close')
                ->count();
        $all = $this->Jobcard_model->where('id_project', '=', $row->Id)->count();
        $persen = round(($close / ($all ? $all : 1)) * 10000) / 100;
        return '<a href="' . base_url() . 'admin/projects/crud_jobcard/' . $row->Id . '">' . $persen . '%</a>';
    }

    public function callback_mdr_progress($value, $row) {
        $close = $this->Mdr_model->where('id_project', '=', $row->Id)
                ->where('status', 'like', 'Close')
                ->count();
        $all = $this->Mdr_model->where('id_project', '=', $row->Id)->count();
        $persen = round(($close / ($all ? $all : 1)) * 10000) / 100;
        return '<a href="' . base_url() . 'admin/projects/crud_mdr/' . $row->Id . '">' . $persen . '%</a>';
    }

    public function check_projectdates($finish_date, $start_date) {
        $parts = explode('/', $this->input->post('start_date'));
        $start_date = join('-', $parts);
        $start_date = strtotime($start_date);

        $parts2 = explode('/', $this->input->post('finish_date'));
        $finish_date = join('-', $parts2);
        $finish_date = strtotime($finish_date);

        if ($finish_date >= $start_date) {

            return true;
        } else {
            $this->form_validation->set_message('check_projectdates', "finish date should be greater than start date");
            return false;
        }
    }

}
